/**
 * Created by panpan on 7/29/14.
 */

var HybridTimeline = function(tree, graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.state = {
        snapshots: []
    };

    self.consts = {
        event: {
            toggle_snapshot: 'toggle_snapshot'
        }
    };

    function init() {

        var g = self.graph,
            nids = g.getNodes(),
            eids = g.getLinks(),
            consts = self.constants;

        for (var i = 0, ii = nids.length; i < ii; i++) {
            g.updateNodeAttrs({
                visual: {
                    x: 0,
                    y: 0,
                    xt: []
                }
            }, nids[i]);
        }

        registerEvents();

    }


    function updateHorizontalPosition() {

        var graph = self.graph,
            config = self.config,
            state = self.state,
            nodes = state.visual.visible_nodes,
            timestamps = graph.timestamps();

        //update horizontal layouts
        //get links at each timestamp and initialize x position
        var nvlinks = [],
            nvx = [];
        for (var i= 0,ii=nodes.length; i<ii; i++) {
            nvlinks.push(graph.adjacentsAggregateByTime(nodes[i]));
            nvx.push(config.dftLeftPadding);
        }

        //clean positioning
        for (var i= 0, ii=nodes.length; i<ii; i++)
            graph.node(nodes[i]).attrs.visual.xt = [];

        //x positioning
        for (var k = 0, kk = timestamps.length; k < kk; k ++) {
            var t = timestamps[k];
            for (var i = 0, ii = nodes.length; i<ii; i++) {
                //assign current positions
                graph.node(nodes[i]).attrs.visual.xt.push({t: t, x: nvx[i]});
                //increment current position
                var el = algo.find_element(t, nvlinks[i], function(d) {return d.t;});
                if (el != null)
                    nvx[i] += el.links.length * config.dftGlyphWidth;

                nvx[i] += config.dftTimeStepPadding * 2;
                nvx[i] += config.dftSeamWidth;
            }

            //leave space for a snapshot
            if (state.visual.snapshots.indexOf(t) > -1) {
                var mx = config.dftSnapShotWidth + 2 * config.dftSnapShotPadding + algo.max(nvx);
                for (var i = 0, ii = nvx.length; i<ii; i++)
                    nvx[i] = mx;
            }

        }
    }

    function renderNodes() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            consts = self.constants,
            state = self.state,
            nids = state.visual.visible_nodes;

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.node')
            .data(nids)
            .enter()
            .append('g')
            .attr('class', 'node')
            .attr('transform', function(nid) {
                return geom.transform.begin().translate(0, graph.node(nid).attrs.visual.y).end();
            })
            .append('rect')
            .attr('class', 'node_substrate')
            .attr('width', config.dftWidth).attr('height', config.dftLineHeight)
            .classed('selected', function(nid) {
               return state.visual.focuses.indexOf(nid) > -1;
            });

        canvas.selectAll('.node')
            .append('text')
            .attr('class', 'node_label')
            .attr('x', config.dftLabelPadding)
            .attr('y', config.dftLineHeight)
            .attr('font-size', config.dftLabelFontSize)
            .attr('color', config.dftLabelFontColor)
            .style('font-family', config.dftFontFamily)
            .text(function(nid) {return graph.node(nid).name;});

    }

    function renderLinks() {

    }

    //update horizontal position of the links
    function updateLinksHPosition() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config;

         canvas.selectAll('.node')
            .each(function (nid) {
                var n = graph.node(nid);
                d3.select(this)
                    .selectAll('.step')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('transform', function(t) {
                        var x = config.dftTimeStepPadding + algo.interpolate_step(t, n.attrs.visual.xt, function(el) {return el.t;}, function(el) {return el.x;}),
                            y = 0;
                        return geom.transform.begin().translate(x, y).end();
                    });
             });
    }


    function renderTimestamps() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.timestamp')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'timestamp')
            .append('path')
            .attr('class', 'seam')
            .attr('d', function(t) {

                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x}),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i< ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x;});

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);

                }

                path.line_to(x, y + config.dftLineHeight);
                return path.end();
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .append('text')
                    .attr('class', 'timestamp_label')
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end())
                    .text(t);

                d3.select(this)
                    .append('path')
                    .attr('class', 'lead')
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );

            });
    }

    function updateTimestampsHPosition() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.selectAll('.timestamp')
            .each(function(t) {
                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x
                        }),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i < ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x;
                        });

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);
                }

                path.line_to(x, y + config.dftLineHeight);

                path = path.end();

                d3.select(this)
                    .select('.seam')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', path);
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .select('.timestamp_label')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end());

                d3.select(this)
                    .select('.lead')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );
            });

    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    
    function setTimestampStatus(t, status, flag, context) {
        d3.select(context).selectAll('.timestamp_label,.seam, .lead').classed(status, flag);
        self.canvas.selectAll('.node').selectAll('.step').filter(function (_t) {
            return _t == t;
        }).selectAll('.step_substrate').classed(status, flag);
    }

    function toggleTimestampStatus(t, status, context) {
        if (d3.select(context).selectAll('.timestamp_label').classed(status)) {
            setTimestampStatus(t, status, false, context);
        }
        else {
            setTimestampStatus(t, status, true, context);
        }
    }

    function registerCallbacks() {

        var snapshots = self.state.visual.snapshots,
            event = self.constants.event,
            state = self.state,
            config = self.config;

        //hovering & click highlights
        self.canvas.selectAll('.node').select('.node_substrate')
            .on('mouseover', function() {
                d3.select(this).classed('highlight', true);
            })
            .on('mouseout', function() {
                d3.select(this).classed('highlight', false);
            })
            .on('click', function() {
                if (d3.select(this).classed('focus')){
                    d3.select(this).classed('focus', false);
                }
                else {
                    d3.select(this).classed('focus', true);
                }
            })
            .on('dblclick', function(nid) {
                if (d3.select(this).classed('selected')) {
                    d3.select(this).classed('selected', false);
                }
                else {
                    d3.select(this).classed('selected', true);
                }

                var focuses = state.visual.focuses;
                if (focuses.indexOf(nid) > -1) {
                    d3.select(this).attr('fill', '#fff');
                    focuses.splice(focuses.indexOf(nid), 1);
                }
                else {
//                    d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
                    focuses.push(nid);
                }
                self.eventDispatcher.fire(event.refocus);

            });


        self.canvas.selectAll('.timestamp').select('.timestamp_label')
            .on('dblclick', null)
            .on('dblclick', function (t) {
                var idx = snapshots.indexOf(t);
                if (idx > -1) {
                    snapshots.splice(idx, 1); snapshots.sort();
                }
                else {
                    snapshots.push(t); snapshots.sort();
                }
                toggleTimestampStatus(t, 'focus', this.parentNode);
                self.eventDispatcher.fire(event.opensnap);
            });

        //hovering & click highlights
        self.canvas.selectAll('.timestamp').selectAll('.timestamp_label, .seam, .lead')
            .on('mouseover', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', true, this.parentNode);
            })
            .on('mouseout', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', false, this.parentNode);
            })
            .on('click', function() {
                var t = d3.select(this.parentNode).datum();
                toggleTimestampStatus(t, 'focus', this.parentNode);
            });


    }

    function registerEvents() {

        self.eventDispatcher.on(self.constants.event.opensnap, function() {
            cleanSnapShots();
            updateHorizontalPosition();
            updateLinksHPosition();
            updateTimestampsHPosition();
            setTimeout(renderSnapshots, self.config.dftTransitionDuration);
        });

        self.eventDispatcher.on(self.constants.event.refocus, function() {
            clean();
            layout();
            render();
            registerCallbacks();
        });

    }

    function display() {
        self.eventDispatcher.fire(self.constants.event.refocus);
    }

    init();

    self.display = display;

}

