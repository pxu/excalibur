/**
 * Created by panpan on 6/7/14.
 */

var Overview = function(graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.cache = {
        links_t: {},
        node_t_degree: {}
    };

    function color(eid) {

        var graph = self.graph,
            cache = self.cache,
            config = self.config,
            e = graph.link(eid),
            n0 = graph.node(e.source),
            n1 = graph.node(e.target),
            t = graph.getLinkAttr('t', eid);

        var d0 = algo.interpolate_step(t - 1, cache.node_t_degree[n0.id], function(el) {return el.t;}, function(el) {return el.degree;}) || 0,
            d1 = algo.interpolate_step(t - 1, cache.node_t_degree[n1.id], function(el) {return el.t;}, function(el) {return el.degree;}) || 0,
            dist = graph.getLinkAttr('dist', eid);

        var x0 = Math.abs(Math.log((d0 + 1)) / Math.log(2)),
            x1 = Math.abs(Math.log((d1 + 1)) / Math.log(2));

        var xx0 = Math.min(1, Math.min(x0, x1) / config.dftMaxLog2Degree),
            xx1 = Math.min(1, Math.max(x0, x1) / config.dftMaxLog2Degree),
            xx2 = Math.min(1, dist / config.dftMaxDistance);

        if (Math.abs(dist+1) < 0.000001) {
            xx2 = 1.0;
        }

        var rr = xx0 * 255,
            gg = xx1 * 255,
            bb = xx2 * 255;

        return d3.rgb(rr, gg, bb).hsl().h;

    }


    function renderTR() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            cache = self.cache,
            ts = graph.timestamps();

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(config.dftLeftPadding, config.dftTopPadding).end())
            .selectAll('.step')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'step')
            .attr('transform', function(t, i) {
                return geom.transform.begin().translate(config.dftStepSize * i, 0).end();
            })
            .each(function(t) {

                var eids = cache.links_t[t];

                algo.sort(eids, function(eid) {return color(eid);});

                d3.select(this)
                    .selectAll('.pixel')
                    .data(eids)
                    .enter()
                    .append('rect')
                    .attr('class', 'pixel')
                    .attr('x', function(eid, j) {return j % config.dftStepSize;})
                    .attr('y', function(eid, j) {return j / config.dftStepSize;})
                    .attr('width', 1)
                    .attr('height', 1)
                    .attr('fill', function(eid) { return d3.hsl(color(eid), 0.8, 0.8).toString();})
                    .attr('fill-opacity', 1.0)
                    .attr('stroke', '#fff')
                    .attr('stroke-width', 0);

            });

    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    function display() {
        renderTR();
    }

    function init() {

        var graph = self.graph,
            nids = graph.getNodes(),
            eids = graph.getLinks(),
            cache = self.cache;

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            cache.node_t_degree[nids[i]] = graph.degreeByTime(nids[i]);
        }

        for (var i = 0, ii = eids.length; i < ii; i ++) {
            var t = graph.getLinkAttr('t', eids[i]);
            if (cache.links_t[t] == undefined) {
                cache.links_t[t] = [];
            }
            cache.links_t[t].push(eids[i]);
        }
    }

    init();

    self.display = display;
}

