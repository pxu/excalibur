var LinkDiagram = function(graph, layout, canvas, params) {

    var self = this;

    self.eventDispatcher = new EventDispatcher();

    self.graph = graph;

    self.config = params;
    self.canvas = canvas;

    //{order: [nids],  coords: {nid0: {x0: ... , y: ...}} }
    //TODO
    self.layout = layout(self.graph);
    self.bundles = graph.bundles(self.layout.order);

    self.id = 'link_diagram_' + Date.now();

    function render() {

        var config = self.config,
            coords = self.layout.coords,
            nids = self.layout.order,
            bdls = self.bundles,
            canvas = self.canvas;

        canvas.append('g')
            .attr('class', 'link_diagram')
            .attr('id', self.id)
            .attr('transform', geom.transform.begin().translate(config.x0, config.y0).end())
            .selectAll('.bundle')
            .data(d3.range(0, bdls.length))
            .enter()
            .append('g')
            .attr('class', 'bundle')
            .append('path')
            .attr('d', function (i) {

                var bdl = bdls[i],
                    ni0 = nids[bdl.i0],
                    ni1 = nids[bdl.i1],
                    nj0 = nids[bdl.j0],
                    nj1 = nids[bdl.j1],
                    path = geom.path.begin(),
                    x = config.width;

                path.move_to(0, coords[ni0].y0)
                    .h_eased_line_to(x, coords[nj0].y0)
                    .line_to(x, coords[nj1].y1)
                    .h_eased_line_to(0, coords[ni1].y1);

                return path.close_path().end();
            })
            .attr('fill', config.linkColor)
            .style('opacity', 0)
            .transition()
            .duration(config.fadeInDuration)
            .style('opacity', config.linkOpacity);
    };

    self.clean = function() {
        canvas.select('#' + self.id).remove();
    };

    self.render = render;

};
