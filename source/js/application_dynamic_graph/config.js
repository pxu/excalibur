/**
 * Created by user on 6/12/14.
 */

var config = {

    visual: {

        overview: {

            dftLeftPadding: 100,
            dftTopPadding: 160,
            dftMaxLog2Degree: 8,
            dftMaxDistance: 5,
            dftStepSize: 50,
            dftPixelSize: 0.60,
            dftFillColor: '#31a354',
            dftQuantizeLevel: 25,
            dftBarWidth: 60,
            dftBarGap: 10

        },

        timeline: {

            dftHeight: 600,
            dftWidth: 20000,

            //parameter for general layout
            dftTopPadding: 80,
            dftLeftPadding: 180,

            //maximum #nodes displayed in detail
            dftNodeLimit: 50,
            dftNodeHalfLimit: 50,

            //parameter for vertical space occupied by each node
            dftLineHeight: 10,
            dftLineDistance: 2.0,

            //parameter for horizontal arrangement to display temporal information
            dftTimeStepPadding: 1.0,

            dftSeamWidth: 0.2,
            dftSeamStrokeColor: '#b4b8b3',
            dftSeamStrokeOpacity: 1.0,

            dftLeadYBuffer: 3.0,
            dftLeadStrokeWidth: 0.8,
            dftLeadStrokeOpacity: 0.8,

            //parameter for displaying snapshot graphs
            dftSnapShotWidth: 160,
            dftSnapShotPadding: 6,
            dftSnapShotLinkColor: '#b4b8d3',
            dftSnapShotLinkOpacity: 0.5,


            //parameter for displaying nodes
            dftNodeStrokeColor: '#fff',
            dftNodeFillColor: '#009393',
            dftNodeFillOpacity: 0.08,
            dftNodeHighlightFillColor: '#acdb04',
            dftNodeHighLightStrokeColor: '#ffffff',
            dftNodeStrokeOpacity: 0.25,
            dftNodeTip: undefined,

            //parameter for displaying glyphs for each edge deletion & creation events
            dftGlyphMaxLength: 8,
            dftGlyphMinLength: 2,
            dftGlyphMaxRotate: 45,
            dftGlyphWidth: 2.5,
            dftGlyphPadding: 0.0,

            dftGlyphStrokeWidth: 0.7,
            dftGlyphStrokeColor: '#222',
            dftGlyphStrokeOpacity: 1.0,
            dftGlyphStrokeFadedOpacity: 0.5,

            dftLabelFontSize: '0.65rem',
            dftLabelFontColor: '#999',
            dftLabelHighLightFontColor: '#e0605c',
            dftLabelFontWeight: 400,
            dftLabelHighlightFontWeight: 600,
            dftLabelPadding: 24,
            dftTimeStampFontHeight: 16,
            dftFontFamily: 'Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif',

            dftTransitionDuration: 1500,

            dftDOI: function() {
                var dftDOIWeights = [600.0, 1.0],
                    dftDOIFunc = [function(d) {return 1.0 / ( d + 1 )}, functools.identity];
                var doi = 0.0;
                for (var i = 0, ii = arguments.length; i < ii; i ++) {
                    doi += dftDOIWeights[i] * dftDOIFunc[i].call(null, arguments[i]);
                }
                return doi;
            },

            dftDegreeQuantizerMax: 8,

            dftDegreeQuantizer: function(d) {
                if (d == 0)
                    return 0;
                var sign = Math.abs(d) / d,
                    d = Math.abs(d),
                    x = Math.ceil(Math.log(d) / Math.log(2));
                x = x > config.visual.timeline.dftDistanceQuantizerMax ? config.visual.timeline.dftDistanceQuantizerMax : x;
                return x * sign;
            },

            dftDistanceQuantizerMax: 5,

            dftDistanceQuantizer: function(d) {
                if (d == -1) {
                    return config.visual.timeline.dftDistanceQuantizerMax;
                }

                if (d <= config.visual.timeline.dftDistanceQuantizerMax - 1)
                    return d;
                else
                    return config.visual.timeline.dftDistanceQuantizerMax;
            },

            dftGlyphLengthScalingFunc: function(x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([1, config.visual.timeline.dftDistanceQuantizerMax])
                    .range([config.visual.timeline.dftGlyphMinLength, config.visual.timeline.dftGlyphMaxLength]);

                return f0(d);

            }

        },

        small_multiples: {

            dftWidth: 1250,
            dftHeight: 750,

            dftLeftPadding: 50,
            dftTopPadding: 50,

            dftHGap: 25,
            dftVGap: 25,

            dftBlockWidth: 200,
            dftBlockHeight: 200,

            dftMaxNodeSize: 8,
            dftMinNodeSize: 1,

            dftNodeFillColor: '#ffffff',

            dftMaxLinkWidth: 4,
            dftMinLinkWidth: 1,


            dftLinkStrokeColor: '#333333',
            dftLinkStrokeHighLightColor: '#d5bc9c',

            dftMaxIteration: 500,

            dftTransitionDuration: 400,

            dftNodeSizeScalingFunc: function(x) {

                var d = config.visual.timeline.dftDegreeQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([0, config.visual.timeline.dftDegreeQuantizerMax])
                    .range([config.visual.small_multiples.dftMinNodeSize, config.visual.small_multiples.dftMaxNodeSize]);

                return f0(d);
            },

            dftLinkWidthScalingFunc: function(x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([0, config.visual.timeline.dftDistanceQuantizerMax])
                    .range([config.visual.small_multiples.dftMinLinkWidth, config.visual.small_multiples.dftMaxLinkWidth]);

                return f0(d);
            },

            dftLinkColorFunc: function (x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain(d3.range(0, config.visual.timeline.dftDistanceQuantizerMax))
                    .range(colorbrewer.YlOrRd[8].slice(4, 7).reverse());

                return f0(d);
            }

        },

        hierarchy: {

            dftWidth: 1000,
            dftHeight: 750,

            dftDendroWidth: 240,
            dftDendroHeight: 700,

            dftTimeStepWidth: 20,

            dftLeftPadding: 50,
            dftTopPadding: 50,

            dftFillColor: '#ed7a3c',
            dftStrokeColor: '#ed7a3c'

        },

        link_diagram: {

            width: 160,
            linkColor: '#b4b8d3',
            linkOpacity: 0.6,
            fadeInDuration: 1000

        }
    }

}
