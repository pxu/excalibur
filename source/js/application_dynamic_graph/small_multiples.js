var SmallMultiples = function(graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.constants = {
        visual: {
            ncolumns : Math.floor((self.config.dftWidth - self.config.dftLeftPadding) / (self.config.dftBlockWidth + self.config.dftHGap))
        },
        event: {
            refocus: 'refocus',
            init: 'init'
        }
    }

    self.state = {
        visual: {
            visible_nodes: []
        },
        cache: {
            subgraph: undefined,
            node_attributes: {}
            // in the form of {nid: {attr0: val0, attr1: val1}},
            // store computed node layout positions, and node degree that varies with time, and nodes' adjacents,

        }
    };

    function init() {
        registerEvents();
    }

    function renderGrids() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            consts = self.constants,
            ts = graph.timestamps();

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(config.dftLeftPadding, config.dftTopPadding).end())
            .selectAll('.step')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'step')
            .attr('transform', function(t, i) {
                var row = Math.floor( i / consts.visual.ncolumns),
                    col = i % consts.visual.ncolumns,
                    x = col * (config.dftBlockWidth + config.dftHGap),
                    y = row * (config.dftBlockHeight + config.dftVGap);

                return geom.transform.begin().translate(x, y).end();
            });

        canvas.selectAll('.step')
            .append('rect')
            .attr('class', 'step_substrate')
            .attr('x', 0).attr('y', 0)
            .attr('width', config.dftBlockWidth).attr('height', config.dftBlockHeight);

    }

    function renderTimestamps() {

        var canvas = self.canvas;

        canvas.selectAll('.step')
            .append('text')
            .attr('class', 'timestamp_label')
            .attr('x', 0).attr('y', -4)
            .text(function(t) {
                return t.toString();
            });

    }

    function renderNLDiagrams() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        canvas.selectAll('.step')
            .append('g')
            .attr('class', 'nodelink')
            .each(function(t) {

                var g = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), t);

                d3.select(this)
                    .append('g')
                    .attr('class', 'links')
                    .selectAll('.link')
                    .data(g.getLinks())
                    .enter()
                    .append('path')
                    .attr('class', 'link')
                    .attr('d', function(eid) {
                        var sid = g.link(eid).source,
                            tid = g.link(eid).target;
                        return geom.path.begin()
                            .move_to(cache.node_attributes[sid].x, cache.node_attributes[sid].y)
                            .line_to(cache.node_attributes[tid].x, cache.node_attributes[tid].y).end();
                    })
                    .attr('stroke-width', function(eid) {
                        if (graph.getLinkAttr('t', eid) == t) {
                            var d = graph.getLinkAttr('dist', eid);
                            return config.dftLinkWidthScalingFunc(d);
                        }
                        else {
                            return config.dftMinLinkWidth;
                        }
                    })
                    .attr('stroke', function(eid) {
                        if (graph.getLinkAttr('t', eid) == t) {
                            var d = graph.getLinkAttr('dist', eid);
                            return config.dftLinkColorFunc(d);
                        }
                        else
                            return config.dftLinkStrokeColor;
                    }
                        );

                d3.select(this)
                    .append('g')
                    .attr('class', 'nodes')
                    .selectAll('.node')
                    .data(g.getNodes().filter(function(nid){return g.degree(nid) > 0;}))
                    .enter()
                    .append('g')
                    .attr('class', 'node')
                    .attr('transform', function(nid) {
                        return geom.transform.begin().translate(
                            cache.node_attributes[nid].x,
                            cache.node_attributes[nid].y
                        ).end();
                    })
                    .append('circle')
                    .attr('class', 'node_circle_drawn')
                    .attr('cx', 0).attr('cy', 0)
                    .attr('r', 6.0);

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .append('circle')
                    .attr('class', 'node_circle')
                    .attr('cx', 0).attr('cy', 0)
                    .attr('r', 16.0);

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .append('text')
                    .attr('x', 8).attr('y', 0)
                    .attr('class', 'node_label')
                    .text(function(nid) {
                        return graph.node(nid).name;
                    });

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .selectAll('.node_circle')
                    .on('mouseover', function(nid) {
                        if (g.degree(nid) == 0) { return; }
                        d3.select(this.parentNode)
                            .classed('highlight_source', true);
                        setNodeStatus(nid, 'highlight', true);

                    })
                    .on('mouseout', function(nid) {
                        d3.select(this.parentNode)
                            .classed('highlight_source', false);
                        setNodeStatus(nid, 'highlight', false);
                    })
                    .on('click', function(nid) {
                        if (d3.select(this.parentNode).classed('focus') || d3.select(this.parentNode).classed('focus_source')) {
                            d3.select(this.parentNode).classed('focus_source', false);
                            d3.select(this.parentNode).classed('focus', false);
                            setNodeStatus(nid, 'focus', false);
                            setNodeStatus(nid, 'focus_source', false);
                        }
                        else {
                            d3.select(this.parentNode).classed('focus_source', true);
                            d3.select(this.parentNode).classed('focus', true);
                            setNodeStatus(nid, 'focus', true);
                        }
                    });
            })
    }

    function updateDataCache() {

        var graph = self.graph,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            cache.node_attributes[nids[i]] = {};
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            cache.node_attributes[nids[i]]['t_degree'] = graph.degreeByTime(nids[i]);
        }

        cache.subgraph = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), graph.t1());

    }

    //use d3 to layout nodes
    function updateLayout() {

        var config = self.config,
            subgraph = self.state.cache.subgraph,
            cache = self.state.cache;

        var rawlinks = subgraph.getLinks(),
            links = [];

        var rawnids = self.state.visual.visible_nodes,
            nodes = [];

        for (var i = 0, ii = rawnids.length; i < ii; i ++) {
            nodes.push({id: rawnids[i]});
        }

        for (var i = 0, ii = rawlinks.length; i < ii; i ++ ) {
            links.push({
                source: subgraph.nodeIdx[subgraph.link(rawlinks[i]).source],
                target: subgraph.nodeIdx[subgraph.link(rawlinks[i]).target]});
        }

        var force = d3.layout.force()
            .gravity(.05)
            .distance(50)
            .charge(-50)
            .size([config.dftBlockWidth, config.dftBlockHeight]);

        force.nodes(nodes)
            .links(links);

        force.start();
        for (var i = 0; i < config.dftMaxIteration; ++i) force.tick();
        force.stop();

        for (var i = 0,  ii = nodes.length; i < ii; i ++) {
            var n = nodes[i];
            cache.node_attributes[n.id].x = n.x;
            cache.node_attributes[n.id].y = n.y;
        }

        geom.scaling(config.dftBlockWidth, config.dftBlockHeight, rawnids,
            function (nid) {
                return cache.node_attributes[nid];
            },
            function (pos, nid) {
                cache.node_attributes[nid].x = pos.x;
                cache.node_attributes[nid].y = pos.y;
            });

    }

    function setNodeStatus(nid, status, flag) {

        var canvas = self.canvas;

        canvas.selectAll('.step')
            .each(function() {
                d3.select(this).selectAll('.nodes')
                    .selectAll('.node')
                    .each(function (_nid) {
                        if (nid == _nid) {
                            d3.select(this)
                                .classed(status, flag);
                        }
                    });
            });
    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    function cleanNLDiagrams() {
        canvas.selectAll('.nodelink').remove();
    }

    function registerEvents() {

        self.eventDispatcher.on(self.constants.event.refocus, function() {
            cleanNLDiagrams();
            updateDataCache();
            updateLayout();
            renderNLDiagrams();
        });

        self.eventDispatcher.on(self.constants.event.init, function() {
            clean();
            renderGrids();
            renderTimestamps();
        });
    }

    function init_display() {
        self.eventDispatcher.fire(self.constants.event.init);
    }

    init();

    self.init_display = init_display;

}

