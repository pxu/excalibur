/**
 * Created by panpan on 6/12/14.
 */

var Timeline = function(graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.state = {
        visual: {
            focuses:  [],
            snapshots: [],
            visible_nodes: [],
            order: {}   //order of the nodes displayed at full
        },
        cache: {
            node_attributes: {} // in the form of {nid: {attr0: val0, attr1: val1}}
        }
    };

    self.constants = {
        visual: {
            hidden: 0,
            half: 1,
            full: 2
        },
        event: {
            refocus: 'refocus',
            opensnap: 'opensnap',
            shuffle: 'shuffle'
        }
    };

    //append visual attributes
    function init() {

        var g = self.graph,
            nids = g.getNodes(),
            eids = g.getLinks(),
            consts = self.constants;

        for (var i = 0, ii = nids.length; i < ii; i++) {
            g.updateNodeAttrs({
                visual: {
                    doi: 0,
                    x: 0,
                    y: 0,
                    xt: [],
                    state: consts.visual.hidden,
                    _x: 0,
                    _y: 0,
                    _xt: [],
                    _state: consts.visual.hidden
                }
            }, nids[i]);
        }

        for (var i = 0, ii = eids.length; i < ii; i++) {
            g.updateLinkAttrs({
                    visual: {
                        x: 0,
                        y: 0,
                        length: 0,
                        angle: 0
                    }
                }, eids[i]);
        }

        registerEvents();

    }


    function updateDOI() {

        var depth = 2,
            graph = self.graph,
            nids = self.graph.getNodes(),
            roots = self.state.visual.focuses,
            config = self.config,
            dists = d3.map();

        for (var i = 0, ii = roots.length; i < ii; i++) {
            var _dists = graph.bfs(roots[i], depth);
            _dists.forEach(function (k, v) {
                if (!dists.has(k)) dists.set(k, v);
                else dists.set(k, Math.min(dists.get(k), _dists.get(k)));
            });
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var n = graph.node(nids[i]);
            n.attrs.visual.doi = graph.degree(nids[i]);
            if (dists.has(n.id)) {
                n.attrs.visual.doi = config.dftDOI.call(null, dists.get(n.id), graph.degree(nids[i]));
            }
        }
    };

    function updateVisualState() {

        var graph = self.graph,
            nids = graph.getNodes(),
            consts = self.constants,
            state = self.state,
            config = self.config;

        var p = algo.permutation(nids, function (nid) {
            return graph.node(nid).attrs.visual.doi;
        }, null, false);

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            if (p[i] < config.dftNodeLimit)
                graph.node(nids[i]).attrs.visual.state = consts.visual.full;
            if (p[i] >= config.dftNodeHalfLimit)
                graph.node(nids[i]).attrs.visual.state = consts.visual.hidden;
        };

        state.visual.visible_nodes = graph.getNodes().filter(function(nid) {return graph.node(nid).attrs.visual.state != consts.visual.hidden});

    };

    //some attributes used in visualization, will not change upon focus change
    function updateDataCache() {

        var graph = self.graph,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        cache.node_attributes = {}; //clear

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var nbrs = graph.neighbors(nids[i]);
            cache.node_attributes[nids[i]] = {};
            for (var j = 0, jj = nbrs.length; j < jj; j++) {
                if (cache.node_attributes[nbrs[j]] == undefined) {
                    cache.node_attributes[nbrs[j]] = {};
                }
            }
        }


        for (var i = 0, ii = nids.length; i < ii; i++) {
            var nbrs = graph.neighbors(nids[i]);
            cache.node_attributes[nids[i]]['t_degree'] = graph.degreeByTime(nids[i]);
            for (var j = 0, jj = nbrs.length; j < jj; j ++) {
                if (cache.node_attributes[nbrs[j]]['t_degree'] == undefined) {
                    cache.node_attributes[nbrs[j]]['t_degree'] = graph.degreeByTime(nbrs[j]);
                }
            }
        }
    }

    function updateLayout() {

        var graph = self.graph,
            state = self.state,
            nids = state.visual.visible_nodes;

        var ag = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), graph.t1()),
            lpm = ag.laplacianMatrix(nids),
            ev = matrix.eigen(lpm);

        var permutation = algo.permutation(ev.V[ev.IDX]);

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            state.visual.order[nids[i]] = permutation[i];
        }
    }

    function updateVerticalPosition() {

        var graph = self.graph,
            config = self.config,
            state = self.state,
            nids = state.visual.visible_nodes,
            p = state.visual.order; //node permutation

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            graph.node(nids[i]).attrs.visual.y = p[nids[i]] * (config.dftLineHeight + config.dftLineDistance);
        }

    }

    function updateHorizontalPosition() {

        var graph = self.graph,
            config = self.config,
            state = self.state,
            nodes = state.visual.visible_nodes,
            timestamps = graph.timestamps();

        //update horizontal layouts
        //get links at each timestamp and initialize x position
        var nvlinks = [],
            nvx = [];
        for (var i= 0,ii=nodes.length; i<ii; i++) {
            nvlinks.push(graph.adjacentsAggregateByTime(nodes[i]));
            nvx.push(config.dftLeftPadding);
        }

        //clean positioning
        for (var i= 0, ii=nodes.length; i<ii; i++)
            graph.node(nodes[i]).attrs.visual.xt = [];

        //x positioning
        for (var k = 0, kk = timestamps.length; k < kk; k ++) {
            var t = timestamps[k];
            for (var i = 0, ii = nodes.length; i<ii; i++) {
                //assign current positions
                graph.node(nodes[i]).attrs.visual.xt.push({t: t, x: nvx[i]});
                //increment current position
                var el = algo.find_element(t, nvlinks[i], function(d) {return d.t;});
                if (el != null)
                    nvx[i] += el.links.length * config.dftGlyphWidth;

                nvx[i] += config.dftTimeStepPadding * 2;
                nvx[i] += config.dftSeamWidth;
            }

            //leave space for a snapshot
            if (state.visual.snapshots.indexOf(t) > -1) {
                var mx = config.dftSnapShotWidth + 2 * config.dftSnapShotPadding + algo.max(nvx);
                for (var i = 0, ii = nvx.length; i<ii; i++)
                    nvx[i] = mx;
            }

        }
    }

    function renderNodes() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            consts = self.constants,
            state = self.state,
            nids = state.visual.visible_nodes;

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.node')
            .data(nids)
            .enter()
            .append('g')
            .attr('class', 'node')
            .attr('transform', function(nid) {
                return geom.transform.begin().translate(0, graph.node(nid).attrs.visual.y).end();
            })
            .append('rect')
            .attr('class', 'node_substrate')
            .attr('width', config.dftWidth).attr('height', config.dftLineHeight)
            .classed('selected', function(nid) {
               return state.visual.focuses.indexOf(nid) > -1;
            });

        canvas.selectAll('.node')
            .append('text')
            .attr('class', 'node_label')
            .attr('x', config.dftLabelPadding)
            .attr('y', config.dftLineHeight)
            .attr('font-size', config.dftLabelFontSize)
            .attr('color', config.dftLabelFontColor)
            .style('font-family', config.dftFontFamily)
            .text(function(nid) {return graph.node(nid).name;});

    }

    function renderLinks() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            state = self.state,
            nids = state.visual.visible_nodes,
            cache = state.cache;

        canvas.selectAll('.node')
            .each(function (nid) {

                var n = graph.node(nid),
                    adjs = graph.adjacentsAggregateByTime(nid),
                    ts = algo.listgen(adjs, function(el) { return el.t;});

                for (var i = 0, ii = adjs.length; i < ii; i++) {
                    var t = adjs[i].t;
                    adjs[i].links.forEach(function (eid) {
                        var e = graph.link(eid),
                            n0id = nid,
                            n1id = graph.neighbor(nid, eid);
                        var d0 = algo.interpolate_step(t - 1, cache.node_attributes[n0id]['t_degree'], function(el) {return el.t;}, function(el) {return el.degree;}) || 0;
                        var d1 = algo.interpolate_step(t - 1, cache.node_attributes[n1id]['t_degree'], function(el) {return el.t;}, function(el) {return el.degree;}) || 0;

                        e.attrs.visual.length = config.dftGlyphLengthScalingFunc(e.attrs['dist']);

                        e.attrs.visual.angle = config.dftDegreeQuantizer(d0 - d1) / config.dftDegreeQuantizerMax * config.dftGlyphMaxRotate;
                    });
                }

                d3.select(this)
                    .selectAll('.step')
                    .data(ts)
                    .enter()
                    .append('g')
                    .attr('class', 'step')
                    .attr('transform', function(t) {
                        var x = config.dftTimeStepPadding + algo.interpolate_step(t, n.attrs.visual.xt, function(el) {return el.t;}, function(el) {return el.x;}),
                            y = 0;
                        return geom.transform.begin().translate(x, y).end();
                    })
                    .append('rect')
                    .attr('class', 'step_substrate')
                    .attr('x', 0).attr('y', 0)
                    .attr('width', function(t, i) {
                        return adjs[i].links.length * config.dftGlyphWidth;
                    })
                    .attr('height', config.dftLineHeight)


                d3.select(this)
                    .selectAll('.step')
                    .append('g')
                    .each(function(t, i) {

                        var eids = adjs[i].links,
                            x0 = 0,
                            p = algo.permutation(eids, function(eid) {return graph.link(eid).attrs.visual.angle;});

                        for (var i = 0, ii = eids.length; i < ii; i++) {
                            var e = graph.link(eids[i]);
                            e.attrs.visual.x = x0 + p[i] * config.dftGlyphWidth + config.dftGlyphWidth / 2;
                        }

                        d3.select(this).selectAll('.link_glyph')
                            .data(eids)
                            .enter()
                            .append('g')
                            .attr('class', 'link_glyph')
                            .attr('transform', function (eid) {
                                var e = graph.link(eid);
                                return geom.transform.begin()
                                    .translate(e.attrs.visual.x, 0)
                                    .rotate(e.attrs.visual.angle, 0, config.dftLineHeight / 2)
                                    .end();
                            })
                            .append('path')
                            .attr('class', 'component0')
                            .attr('d', function () {
                                return geom.path.begin()
                                    .move_to(0, (config.dftLineHeight - config.dftGlyphMaxLength) / 2)
                                    .vertical_to_relative(config.dftGlyphMaxLength)
                                    .end();
                            });

                        d3.select(this).selectAll('.link_glyph')
                            .append('path')
                            .attr('class', 'component1')
                            .attr('d', function(eid) {
                                var e = graph.link(eid);
                                return geom.path.begin()
                                    .move_to(0, (config.dftLineHeight - e.attrs.visual.length) / 2)
                                    .vertical_to_relative(e.attrs.visual.length)
                                    .end();
                            });
                    });
            });
    }

    //update horizontal position of the links
    function updateLinksHPosition() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config;

         canvas.selectAll('.node')
            .each(function (nid) {
                var n = graph.node(nid);
                d3.select(this)
                    .selectAll('.step')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('transform', function(t) {
                        var x = config.dftTimeStepPadding + algo.interpolate_step(t, n.attrs.visual.xt, function(el) {return el.t;}, function(el) {return el.x;}),
                            y = 0;
                        return geom.transform.begin().translate(x, y).end();
                    });
             });
    }


    function renderSnapshots() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            state = self.state,
            snapshots = state.visual.snapshots,
            nids = state.visual.visible_nodes;

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .attr('class', 'snapshots')
            .selectAll('.snapshot')
            .data(snapshots)
            .enter()
            .append('g')
            .attr('class', 'snapshot')
            .attr('transform', function(t) {
                return geom.transform.begin()
                    .translate(algo.interpolate_step(t+1, graph.node(nids[0]).attrs.visual.xt, function(el){return el.t;}, function(el){return el.x;}) - config.dftSnapShotWidth -config.dftSnapShotPadding, 0)
                    .end();
            });

        //clean space
        canvas.selectAll('.snapshot')
            .append('rect')
            .attr('x', 0)
            .attr('y', config.dftTopPadding)
            .attr('width', config.dftSnapShotWidth)
            .attr('height', config.dftHeight)
            .attr('fill', '#fff');

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.selectAll('.snapshot')
            .each(function (t) {

                var g = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), t),
                    bdls = g.bundles(nids);

                d3.select(this).selectAll('.bundle')
                    .data(d3.range(0, bdls.length))
                    .enter()
                    .append('g')
                    .attr('class', 'bundle')
                    .each(function(i) {

                        //multiple edges in a bundle
                        if (bdls[i].i0 != bdls[i].i1 || bdls[i].j0 != bdls[i].j1) {

                            d3.select(this)
                                .append('path')
                                .attr('class', 'multiple')
                                .attr('d', function (i) {

                                    var bdl = bdls[i],
                                        ni0 = graph.node(nids[bdl.i0]),
                                        ni1 = graph.node(nids[bdl.i1]),
                                        nj0 = graph.node(nids[bdl.j0]),
                                        nj1 = graph.node(nids[bdl.j1]),
                                        path = geom.path.begin(),
                                        x = config.dftSnapShotWidth;

                                    if (bdls[i].i0 == bdls[i].i1) {

                                        path.move_to(0, ni0.attrs.visual.y + config.dftLineHeight / 2)
                                            .h_eased_line_to(x, nj0.attrs.visual.y)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight / 2)
                                            .close_path();

                                    }
                                    else if (bdls[i].j0 == bdls[i].j1) {

                                        path.move_to(0, ni0.attrs.visual.y)
                                            .h_eased_line_to(x, nj0.attrs.visual.y + config.dftLineHeight / 2)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight / 2)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight)
                                            .close_path();

                                    }
                                    else {

                                        path.move_to(0, ni0.attrs.visual.y)
                                            .h_eased_line_to(x, nj0.attrs.visual.y)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight)
                                            .close_path();

                                    }

                                    return path.end();
                                })
                                .attr('fill', config.dftSnapShotLinkColor)
                                .attr('stroke-opacity', 0.0)
                                .style('opacity', 0)
                                .transition()
                                .duration(config.dftTransitionDuration)
                                .style('opacity', config.dftSnapShotLinkOpacity);

                        }
                        else {

                            d3.select(this)
                                .append('path')
                                .attr('class', 'single')
                                .attr('transform', geom.transform.begin().translate(0, config.dftLineHeight / 2).end())
                                .attr('d', function (i) {

                                    var bdl = bdls[i],
                                        ni0 = graph.node(nids[bdl.i0]),
                                        nj0 = graph.node(nids[bdl.j0]),
                                        path = geom.path.begin(),
                                        x = config.dftSnapShotWidth;

                                    path.move_to(0, ni0.attrs.visual.y)
                                        .h_eased_line_to(x, nj0.attrs.visual.y);

                                    return path.end();
                                })
                                .attr('stroke', config.dftSnapShotLinkColor)
                                .attr('stroke-opacity', 0.0)
                                .transition()
                                .duration(config.dftTransitionDuration)
                                .style('stroke-opacity', config.dftSnapShotLinkOpacity);

                        }
                    });
            })

    }

    function cleanSnapShots() {
        d3.selectAll('.snapshots').remove();
    }

    function renderTimestamps() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.timestamp')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'timestamp')
            .append('path')
            .attr('class', 'seam')
            .attr('d', function(t) {

                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x}),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i< ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x;});

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);

                }

                path.line_to(x, y + config.dftLineHeight);
                return path.end();
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .append('text')
                    .attr('class', 'timestamp_label')
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end())
                    .text(t);

                d3.select(this)
                    .append('path')
                    .attr('class', 'lead')
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );

            });
    }

    function updateTimestampsHPosition() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.selectAll('.timestamp')
            .each(function(t) {
                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x
                        }),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i < ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x;
                        });

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);
                }

                path.line_to(x, y + config.dftLineHeight);

                path = path.end();

                d3.select(this)
                    .select('.seam')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', path);
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .select('.timestamp_label')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end());

                d3.select(this)
                    .select('.lead')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );
            });

    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    function render() {
        renderNodes();
        renderLinks();
        renderSnapshots();
        renderTimestamps();
    }

    function refocus() {
        updateDOI();
        updateVisualState();
        updateDataCache();
    }

    function layout() {
        updateLayout();
        updateVerticalPosition();
        updateHorizontalPosition();
    }

    function setTimestampStatus(t, status, flag, context) {
        d3.select(context).selectAll('.timestamp_label,.seam, .lead').classed(status, flag);
        self.canvas.selectAll('.node').selectAll('.step').filter(function (_t) {
            return _t == t;
        }).selectAll('.step_substrate').classed(status, flag);
    }

    function toggleTimestampStatus(t, status, context) {
//        console.info(d3.select(context).selectAll('.timestamp_label'));
        if (d3.select(context).selectAll('.timestamp_label').classed(status)) {
            setTimestampStatus(t, status, false, context);
        }
        else {
            setTimestampStatus(t, status, true, context);
        }
    }

//    function setNodeStatus(t)

    function registerCallbacks() {

        var snapshots = self.state.visual.snapshots,
            event = self.constants.event,
            state = self.state,
            config = self.config;

//        self.canvas.selectAll('.node').select('.node_substrate')
//            .on('mouseover', function () {
//                d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
//            })
//            .on('mouseout', function (nid) {
//                var focuses = state.visual.focuses;
//                if (focuses.indexOf(nid) == -1) {
//                    d3.select(this).attr('fill', '#fff');
//                }
//            })
//            .on('dblclick', null)
//            .on('dblclick', function (nid) {
//                var focuses = state.visual.focuses;
//                if (focuses.indexOf(nid) > -1) {
//                    d3.select(this).attr('fill', '#fff');
//                    focuses.splice(focuses.indexOf(nid), 1);
//                }
//                else {
//                    d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
//                    focuses.push(nid);
//                }
//                self.eventDispatcher.fire(event.refocus);
//            });

        //hovering & click highlights
        self.canvas.selectAll('.node').select('.node_substrate')
            .on('mouseover', function() {
                d3.select(this).classed('highlight', true);
            })
            .on('mouseout', function() {
                d3.select(this).classed('highlight', false);
            })
            .on('click', function() {
                if (d3.select(this).classed('focus')){
                    d3.select(this).classed('focus', false);
                }
                else {
                    d3.select(this).classed('focus', true);
                }
            })
            .on('dblclick', function(nid) {
                if (d3.select(this).classed('selected')) {
                    d3.select(this).classed('selected', false);
                }
                else {
                    d3.select(this).classed('selected', true);
                }

                var focuses = state.visual.focuses;
                if (focuses.indexOf(nid) > -1) {
                    d3.select(this).attr('fill', '#fff');
                    focuses.splice(focuses.indexOf(nid), 1);
                }
                else {
//                    d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
                    focuses.push(nid);
                }
                self.eventDispatcher.fire(event.refocus);

            });


        self.canvas.selectAll('.timestamp').select('.timestamp_label')
            .on('dblclick', null)
            .on('dblclick', function (t) {
                var idx = snapshots.indexOf(t);
                if (idx > -1) {
                    snapshots.splice(idx, 1); snapshots.sort();
                }
                else {
                    snapshots.push(t); snapshots.sort();
                }
                toggleTimestampStatus(t, 'focus', this.parentNode);
                self.eventDispatcher.fire(event.opensnap);
            });

        //hovering & click highlights
        self.canvas.selectAll('.timestamp').selectAll('.timestamp_label, .seam, .lead')
            .on('mouseover', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', true, this.parentNode);
            })
            .on('mouseout', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', false, this.parentNode);
            })
            .on('click', function() {
                var t = d3.select(this.parentNode).datum();
                toggleTimestampStatus(t, 'focus', this.parentNode);
            });


    }

    function registerEvents() {

        self.eventDispatcher.on(self.constants.event.opensnap, function() {
            cleanSnapShots();
            updateHorizontalPosition();
            updateLinksHPosition();
            updateTimestampsHPosition();
            setTimeout(renderSnapshots, self.config.dftTransitionDuration);
//            renderSnapshots();
        });

        self.eventDispatcher.on(self.constants.event.refocus, function() {
            clean();
            refocus();
            layout();
            render();
            registerCallbacks();
        });

    }

    function display() {
        self.eventDispatcher.fire(self.constants.event.refocus);
    }

    init();

    self.display = display;

}




