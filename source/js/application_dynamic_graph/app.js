/**
 * Created by panpan on 6/7/14.
 */

var app = {};

var drawHierarchy = function() {

    d3.json('data/graph/dblp_vis_coauthor_community_layout.json', function (data) {
        var g = new Graph();
        for (var i = 0, ii = data.links.length; i < ii; i ++) {
            g.addLink(i, data.links[i][0], data.links[i][1], data.links[i][2]);
        }
        app.canvas_hierarchy  = d3.select('#hierarchy').append('svg')
            .style('width', '100%')
            .style('height', '98%');
        app.hierarchy = new Dendrogram(data.tree, g, app.graph, app.canvas_hierarchy, config.visual.hierarchy);
        app.hierarchy.render();
    });

}

$(function(){
    drawHierarchy();
});







