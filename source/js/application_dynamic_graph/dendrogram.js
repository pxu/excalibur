/*
 * Created by panpan
 */

var Dendrogram = function(tree, cgraph, graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.root = tree;
    self.cgraph = cgraph;

    self.config = params;
    self.canvas = canvas;

    function isLeaf(n) {
        return n.children == undefined;
    }

    function left(n) {
        if (!isLeaf(n))
            return n.children[0];
        else
            return null;
    }

    function right(n) {
        if (!isLeaf(n))
            return n.children[1];
        else
            return null;
    }

    function leafInstance(n) {
        if (isLeaf(n)) { return n;}
        else {return leafInstance(left(n));}
    }

    function leaves_id(n) {
        if (n == null) return [];
        if (isLeaf(n)) return [n.id];
        return leaves_id(left(n)).concat(leaves_id(right(n)));
    }

    function leaves(n) {
        if (n == null) return [];
        if (isLeaf(n)) return [n];
        return leaves(left(n)).concat(leaves(right(n)));
    }

    function nonLeaves(n) {
        if (n == null) return [];
        if (isLeaf(n)) return [];
        else return nonLeaves(left(n)).concat(nonLeaves(right(n))).concat([n]);
    }

    function weight(n) {
        if (n == null) return 0;
        if (isLeaf(n)) return n.fans.length;
        return weight(left(n)) + weight(right(n));
    }

    function reposition(n) {
        if (n == null || isLeaf(n)) return;
        else {
            reposition(left(n));
            reposition(right(n));
            n.x = (left(n).x * weight(right(n)) + right(n).x * weight(left(n))) / weight(n);
        }
    }

    function distFunc(nid0, nid1) {
        var cgraph = self.cgraph;
        var eid = cgraph.link(nid0, nid1);
        if (eid == undefined)
            return 0;
        else
            return cgraph.getLinkAttr('weight', eid);
    }

    //swap child nodes in the tree
    function layout() {
        var order = leafOrder(distFunc, isLeaf, left, right).call(null, self.root);
        var orderIdx = {};
        for (var i= 0, ii = order.length; i < ii; i ++) {
            orderIdx[order[i]] = i;
        }

        var innodes = nonLeaves(self.root);
        for (var i= 0, ii = innodes.length; i < ii; i++) {
            var n = innodes[i];

            var lid0 = leafInstance(left(n)).id,
                lid1 = leafInstance(right(n)).id;

            if (orderIdx[lid0] > orderIdx[lid1]) {
                var tmp = left(n);
                n.children[0] = right(n);
                n.children[1] = tmp;
            }
        }
    }

    function render() {

        var config = self.config,
            root = self.root;

        var tree = d3.layout.cluster()
            .size([config.dftDendroHeight, config.dftDendroWidth]);

        var nodes = tree.nodes(root),
            links = tree.links(nodes),
            lvs = leaves(root);

        var wscale = (config.dftDendroHeight  - lvs.length) / weight(root);

        var x = 0;
        for (var i = 0, ii = lvs.length; i < ii; i ++) {
            x += weight(lvs[i]) * wscale / 2 + 0.5;
            lvs[i].x = x;
            x += weight(lvs[i]) * wscale / 2 + 0.5;
        }

        reposition(root);

        canvas.append("g")
            .attr('class', 'dendrogram')
            .selectAll('.link')
            .data(links)
            .enter().append("path")
            .attr("class", "link")
            .attr("d", function (d) {
                return geom.path.begin().move_to(d.source.y, d.source.x)
                    .vertical_to(d.target.x).horizontal_to(d.target.y).end();
            });

        canvas.select('.dendrogram')
            .selectAll('.node')
            .data(nodes.filter(function(n){return isLeaf(n);}))
            .enter().append("g").attr("class", "node")
            .append('path')
            .attr("d", function (n) {
                var delta = weight(n) * wscale / 2;
                return geom.path.begin()
                    .move_to(n.y, n.x - delta)
                    .line_to(n.y, n.x + delta).end();
            });
    }


    self.layout = layout;
    self.render = render;

}


