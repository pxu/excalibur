//node link graph
//author: panpan
//contact: xpp2007@gmail.com
NLGraph = function (graph, canvas, params) {

    var self = this;

    this.graph = graph;

    this.vis = {};

    this.vis.canvas = d3.select(element);

    this.vis.config = {
        width: $(element).width(),
        height: $(element).height(),
        node_size: 8,
        node_fill: '#ccc',
        node_fill_opacity: 0.8,
        node_stroke: '#fff',
        node_stroke_width: 2.0,
        link_stroke: '#9d9cc9',
        link_stroke_width: 1.0,
        link_stroke_opacity: 0.1
    };

    function display() {

        var canvas = self.vis.canvas;
        var g = self.graph;
        var c = self.vis.config;

        geom.scaling(c.width, c.height, g.getNodes(),
            function(nid){return g.getNodeAttrs(['x', 'y'], nid)},
            function(p, nid){ g.updateNodeAttrs(p, nid);});

        canvas.append('g').selectAll('.link')
            .data(g.getLinks())
            .enter()
            .append('path')
            .attr('class', 'link')
            .attr('d', function(eid){
                var src = g.link(eid).source,
                    tgt = g.link(eid).target;
                return geom.path.begin()
                    .move_to(g.getNodeAttr('x', src), g.getNodeAttr('y', src))
                    .line_to(g.getNodeAttr('x', tgt), g.getNodeAttr('y', tgt))
                    .end();
            })
            .attr('stroke', c.link_stroke)
            .attr('stroke-width', c.link_stroke_width)
            .attr('stroke-opacity', c.link_stroke_opacity)
            .attr('fill-opacity', 0);

        canvas.append("g").selectAll(".node")
            .data(g.getNodes())
            .enter()
            .append('circle')
            .attr('class', 'node')
            .attr('cx', function (nid) {
                return g.getNodeAttr('x', nid);
            })
            .attr('cy', function (nid) {
                return g.getNodeAttr('y', nid);
            })
            .attr('r', function (nid) {
                return g.degree(nid);
            })
            .attr('fill', c.node_fill)
            .attr('fill-opacity', c.node_fill_opacity)
            .attr('stroke', c.node_stroke)
            .attr('stroke-width', c.node_stroke_width);

    }


    function clean() {
        //TODO: clean stuffs on canvas
    };

    this.display = display;
    this.clean = clean;

};
