/**
 * some adaptions made
 */

/**
 * optimal dendrogram ordering
 *
 * implementation of binary tree ordering described in [Bar-Joseph et al., 2003]
 * by Renaud Blanch.
 * JavaScript translation by Jean-Daniel Fekete.
 *
 * [Bar-Joseph et al., 2003]
 * K-ary Clustering with Optimal Leaf Ordering for Gene Expression Data.
 * Ziv Bar-Joseph, Erik D. Demaine, David K. Gifford, Angèle M. Hamel,
 * Tommy S. Jaakkola and Nathan Srebro
 * Bioinformatics, 19(9), pp 1070-8, 2003
 * http://www.cs.cmu.edu/~zivbj/compBio/k-aryBio.pdf
 */

var leafOrder = function (_distFunc, _isLeaf, _left, _right) {

    var leavesMap = {},
        orderMap = {},
        distFunc = _distFunc,
        isLeaf = _isLeaf,
        left = _left,
        right = _right;

    function leaves(n) {
        if (n == null) return [];
        if (n.id in leavesMap)
            return leavesMap[n.id];
        return (leavesMap[n.id] = _leaves(n));
    }

    function _leaves(n) {
        if (n == null) return [];
        if (isLeaf(n)) return [n.id];
        return leaves(left(n)).concat(leaves(right(n)));
    }

    function order(v, i, j) {
        var key = "k" + v.id + "-" + i + "-" + j; // ugly key
        if (key in orderMap)
            return orderMap[key];
        return (orderMap[key] = _order(v, i, j));
    }

    function _order(v, i, j) {

        if (isLeaf(v))
            return [0, [v.id]];

        var l = left(v), r = right(v);
        var L = leaves(l), R = leaves(r);

        var w, x;
        if (L.indexOf(i) != -1 && R.indexOf(j) != -1) {
            w = l;
            x = r;
        }
        else if (R.indexOf(i) != -1 && L.indexOf(j) != -1) {
            w = r;
            x = l;
        }
        else
            throw {
                error: "Node is not common ancestor of " + i + ", " + j
            };

        var Wl = leaves(left(w)), Wr = leaves(right(w));
        var Ks = Wr.indexOf(i) != -1 ? Wl : Wr;
        if (Ks.length == 0)
            Ks = [i];

        var Xl = leaves(left(x)), Xr = leaves(right(x));
        var Ls = Xr.indexOf(j) != -1 ? Xl : Xr;
        if (Ls.length == 0)
            Ls = [j];

        var max = 0, optimal_order = [];

        for (var k = 0; k < Ks.length; k++) {

            var w_max = order(w, i, Ks[k]);
            for (var l = 0; l < Ls.length; l++) {
                var x_max = order(x, Ls[l], j);

                var sim = w_max[0] + distFunc(Ks[k], Ls[l]) + x_max[0];
                if (sim > max) {
                    max = sim;
                    optimal_order = w_max[1].concat(x_max[1]);
                }
            }
        }
        return [max, optimal_order];
    }

    function orderFull(v) {

        var max = 0,
            optimal_order = [],
            leftLeaves = leaves(left(v)),
            rightLeaves = leaves(right(v));

        for (var i = 0; i < leftLeaves.length; i++) {
            for (var j = 0; j < rightLeaves.length; j++) {

                var so = order(v, leftLeaves[i], rightLeaves[j]);
                if (so[0] > max) {
                    max = so[0];
                    optimal_order = so[1];
                }

            }
        }

        return optimal_order;
    };

    function reorder(root) {
        return orderFull(root);
    };

    return reorder;
};