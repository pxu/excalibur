/**
 * Created by panpan on 5/26/14. Mostly graph layout algorithms
 */
var layout = function() {
    var l = {
        version: 0.0
    };

    l.mds = function(graph) {
        //TODO: ? a webgl version of mds
    };

    l.spectral = function(graph) {
        //TODO: ? spectral coordinates of the nodes
    };

    l.space_filling_curve = function(graph) {
        //TODO: ? a space filling curve based graph layout
    };

    //create a triangle tiling layout of a graph
    l.triangle_tiling = function(graph) {

    };

    return l;
}();