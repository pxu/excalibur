/**
 * Created by panpan on 6/8/14.
 */

var DynamicGraph = function() {

    Graph.call(this);

};

_.extend(DynamicGraph.prototype, Graph.prototype, {

    t0: function() {
        return algo.min(this.getLinks(), function(eid) {return this.getLinkAttr('t', eid);}, this);
    },

    t1: function() {
        return algo.max(this.getLinks(), function(eid) {return this.getLinkAttr('t', eid)}, this);
    },

    timerange: function(){
        return {t0: this.t0(), t1: this.t1()};
    },

    timestamps: function() {

        var t = this.t0(),
            links = this.getLinks(),
            o = algo.order(links, function (eid) {return this.getLinkAttr('t', eid);}, this);

        var ts = [t];

        for (var i = 0, ii = links.length; i < ii; i += 1) {
            var eid = links[o[i]],
                _t = this.getLinkAttr('t', eid);
            if (_t != t) {
                ts.push(_t);
                t = _t;
            }
        }

        return ts;
    },

    //TODO: use different methods
    //for large/small number of nodes
    getTemporalAggregatedSubGraph: function(nids, tbgn, tend) {

        var g = new Graph(),
            eids = [];

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = ii; j < jj; j ++) {
                var e = this.link(nids[i], nids[j]);
                if (_.isArray(e)) {
                    eids = eids.concat(e);
                }
                else {
                    if (e != null){
                        eids.push(e);
                    }
                }
            }
        }

        eids = _.filter(eids,
            function(eid) {
                var t = this.getLinkAttr('t', eid);
                return t >= tbgn && t <= tend;
            },
            this);

        for (var i = 0, ii = nids.length; i < ii; i++) {
            g.addNode(nids[i]);
        }

        for (var i = 0, ii = eids.length; i < ii; i++) {
            g.addLink(eids[i], this.link(eids[i]).source, this.link(eids[i]).target);
        }

        return g;
    },

    adjacentsAggregateByTime: function(nid) {

        var adjs = this.adjacents(nid),
            o = algo.order(adjs, function (eid) {
            return this.getLinkAttr('t', eid);
        }, this, true);

        var eidst = [],
            lastT = this.t0()-1,
            lastLinks = undefined;

        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            var e = this.link(adjs[o[i]]),
                t = this.getLinkAttr('t', adjs[o[i]]);
            if (t != lastT) {
                lastT = t;
                lastLinks = [];
                eidst.push({t: lastT, links: lastLinks});
            }
            lastLinks.push(e.id);
        }

        return eidst;
    },

    degreeByTime: function(nid) {

        var degrees = [],
            adjs = this.adjacentsAggregateByTime(nid);

        var degree = 0;
        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            degree += adjs[i].links.length;
            degrees.push({t: adjs[i].t, degree: degree});
        }

        return degrees;
    }
});

/* Add overloaded function */
functools.addMethod(DynamicGraph.prototype, 'adjacents',
    function(nid, tbgn, tend){

        var adjs = this.adjacents(nid);

        return _.filter(adjs,
            function (eid) {
                var t = this.getLinkAttr('t', eid);
                return t >= tbgn && t <= tend;
            },
            this);
    });

functools.addMethod(DynamicGraph.prototype, 'neighbors',
    function(nid, tbgn, tend){

        var adjs = this.adjacents(nid, tbgn, tend),
            rs = [];

        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            rs.push(this.neighbor(nid, adjs[i]));
        }

        return rs;
    });



