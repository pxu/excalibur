/**
 * Created by user on 4/16/14.
 */
color = function() {
    var c = {
        version: '0.0',
        //CIE RGB to CIE xyY conversion
        rgb_to_xyY: function(r, g, b) {
            if (!(r<1.0 && g<1.0 && b<1.0)) {
                r = r / 255;
                g = g / 255;
                b = b / 255;
            }
            var s = 1/0.17697,
                X = s * (0.49 * r + 0.31*g + 0.20 * b),
                Y = s * (0.17697 * r + 0.81240*g + 0.01063* b),
                Z = s * (0.00 * r + 0.01 * g + 0.99 * b);
            var xx = X / (X + Y + Z),
                yy = Y / (X + Y + Z);
            return {x: xx, y: yy , Y: Y};
        },
        xyY_to_rgb: function(x, y, YY) {
            var X = YY / y * x,
                Z = YY / y * (1 - x - y);
            var r = 0.41847 * X + (-0.15866) * YY + (-0.082835) * Z,
                g = (-0.091169) * X + 0.25243 * YY + 0.015708 * Z,
                b = 0.00092090 * X + (-0.0025498) * YY + 0.17860 * Z;
            return {r: r * 255, g: g * 255, b: b * 255};
        }
    }
    return c;
}();
