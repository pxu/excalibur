var renderDesityMap = function(graph, width, height) {

    var canvas = $('#display canvas')[0];

    var ndata = graph.getNodes().length;

    var colorTexture = glColor.getYToGTexture();

    var stdev = 6.0,
        nGrid = Math.floor(stdev * 4),
        kernelTexture;

    var kernelData = new Float32Array(nGrid * 1);

    var sum = 0.0;
    for (var i = 0, ii= kernelData.length; i < ii; i++ ) {
        kernelData[i] = Math.exp(- Math.pow(i - (ii - 1) / 2 , 2) / Math.pow(stdev, 2) / 2);
        sum += kernelData[i];
    }

    for (var i = 0, ii = kernelData.length; i < ii; i ++) {
        kernelData[i] /= sum;
    }

    kernelTexture = glUtil.generateTexture(kernelData, THREE.AlphaFormat, THREE.FloatType, nGrid, 1);

	var cameraRTT = new THREE.OrthographicCamera(0, width, height,
0, -100, 100);
    cameraRTT.position.z = 100;

    var camera = new THREE.PerspectiveCamera(60,  width/height, 0, 100);
    camera.position.z = 100;

    canvas.style.width = width / window.devicePixelRatio;
    canvas.style.height = height / window.devicePixelRatio;
    canvas.width = width;
    canvas.height = height;
    var renderer = new THREE.WebGLRenderer({alpha: true, canvas: canvas, preserveDrawingBuffer: true});
    renderer.autoClearColor = false;

    var fragmentShaderVConvolution = new THREE.ShaderMaterial({
        uniforms: {
            resolution: { type: "v2", value: new THREE.Vector2(width, height) },
            textureDensity: { type: "t", value: null },
            textureKernel: {type: "t", value: kernelTexture}
        },
		defines: {
			uNGrid: nGrid.toFixed(2)
		},
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_vconvolution,
        transparent: true
    });


    var fragmentShaderHConvolution = new THREE.ShaderMaterial({
        uniforms: {
            resolution: { type: "v2", value: new THREE.Vector2(width, height) },
            textureDensity: { type: "t", value: null },
            textureKernel: {type: "t", value: kernelTexture}
        },
		defines: {
			uNGrid: nGrid.toFixed(2)
		},
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_hconvolution,
        transparent: true
    });


    var fragmentShaderColorMapping = new THREE.ShaderMaterial({
        uniforms: {
            resolution: {type: "v2", value: (new THREE.Vector2(width, height))},
            uNData: {type: "f", value: ndata},
            uColor: {type: "v3", value: (new THREE.Vector3(color.r, color.g, color.b))},
            textureDensity: {type: "t", value: null},
            textureColor: {type: "t", value: colorTexture}
        },
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_color_mapping,
        transparent: true
    });

    var rtDensity1 = glUtil.getRenderTarget(width, height, THREE.RGBAFormat, THREE.FloatType),
        rtDensity2 = rtDensity1.clone();

    var stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    $('#display').append(stats.domElement);

    //add data points to geometry
    var points = new THREE.Geometry(),
        pmaterial = new THREE.ParticleSystemMaterial({
            color: 0xFFFFFF,
            size: 1,
            blending: THREE.CustomBlending,
            blendEquation: THREE.AddEquation,
            blendSrc: THREE.OneFactor,
            blendDst: THREE.OneFactor,
            opacity: 0.075,
            transparent: true
        });

    for (var i= 0, nodes = graph.getNodes(), ii = nodes.length; i < ii; i++) {

        var pX = graph.getNodeAttr('x', nodes[i]),
            pY = graph.getNodeAttr('y', nodes[i]),
            pZ = 0,
            point = new THREE.Vector3(pX, pY, pZ);

        points.vertices.push(point);
    }


    var particleSystem = new THREE.ParticleSystem(points, pmaterial);
    var pointsScene = new THREE.Scene();
    pointsScene.add(particleSystem);

    var renderAccumulate = function(output) {
        renderer.render(pointsScene, cameraRTT, output, true);
    };

    var scene = new THREE.Scene();
    var rect = new THREE.PlaneGeometry(width, height),
        mesh = new THREE.Mesh(rect, fragmentShaderColorMapping);
    scene.add(mesh);

    var renderVConvolution = function(input, output) {
        mesh.material = fragmentShaderVConvolution;
        fragmentShaderVConvolution.uniforms.textureDensity.value = input;
        renderer.render(scene, cameraRTT, output, true);
    };

    var renderHConvolution = function(input, output) {
        mesh.material = fragmentShaderHConvolution;
        fragmentShaderHConvolution.uniforms.textureDensity.value = input;
        renderer.render(scene, cameraRTT, output, true);
    };

    //build graph scene
    var lines = new THREE.Geometry(),
        lmaterial = new THREE.LineBasicMaterial(
        {
            color: 0xffffff,
            opacity: 0.02,
            transparent: true,
            linewidth: 1,
            blending: THREE.AdditiveBlending
        }
    );

    for (var i = 0, links = graph.getLinks(), ii = links.length; i < ii; i ++) {
        var e = graph.link(links[i]),
            n0 = graph.node(e.source),
            n1 = graph.node(e.target);

        lines.vertices.push(
            new THREE.Vector3(n0.attrs.x, n0.attrs.y, 0.0));
        lines.vertices.push(
            new THREE.Vector3(n1.attrs.x, n1.attrs.y, 0.0));
    }

    var edges = new THREE.Line(lines, lmaterial, THREE.LinePieces);
    var edgesScene = new THREE.Scene();
    edgesScene.add(edges);

    var renderColorMap = function(input) {
        mesh.material = fragmentShaderColorMapping;
        fragmentShaderColorMapping.uniforms.textureDensity.value = input;
        renderer.render(scene, camera);

    };

    var renderEdges = function() {
        renderer.render(edgesScene, cameraRTT, null, false);
    };

    var render = function () {

        requestAnimationFrame(render);

        renderer.clear(true, true, true);
        renderAccumulate(rtDensity1);
        renderVConvolution(rtDensity1, rtDensity2);
        renderer.clearTarget(rtDensity1, true, true, true);

        renderHConvolution(rtDensity2, rtDensity1);
        renderer.clearTarget(rtDensity2, true, true, true);

        renderColorMap(rtDensity1);
        renderer.clearTarget(rtDensity1, true, true, true);
        stats.update();

        renderEdges();
    };

    render();

}

$(function() {


    //TODO: load graph and show edges in 3D space
    var graph = new Graph();

    //DBLP co-authorship graph
    d3.json('data/dblp_vis_coauthor.json', function(data) {

        for (var i= 0, ii=data.nodes.length; i < ii; i++) {
            graph.addNode(data.nodes[i].id, data.nodes[i]);
        }

        for (var i= 0, ii=data.links.length; i < ii; i++) {
            graph.addLink(i, data.nodes[data.links[i].source].id, data.nodes[data.links[i].target].id, data.links[i]);
        }

        for (var i= 0, nodes = graph.getNodes(), ii = nodes.length; i < ii; i++) {
            var n = graph.node(nodes[i]);
            n.attrs.x = n.attrs.x / 100 + 250 + 250;
            n.attrs.y = n.attrs.y / 100 + 250;
        }

        renderDesityMap(graph, 1024, 512);

    });

});



