/**
 * Created by panpan on 4/16/14.
 */
functools = function () {
    var f = {
        version: '0.0',
        identity: function(x) {return x;},
        //addMethod (for javascript function overload)  - By John Resig (MIT Licensed)
        addMethod: function (object, name, fn) {
            var old = object[ name ];
            object[ name ] = function () {
                if (fn.length == arguments.length)
                    return fn.apply(this, arguments);
                else if (typeof old == 'function')
                    return old.apply(this, arguments);
            };
        }
    };
    return f;
}();

algo = function () {

    var al ={
        version: "0.0"
    };

    //list generator from another list
    al.listgen = function(array, accessor, context) {

        context = context || null;

        var rs = [];
        for (var i= 0, len=array.length; i < len; i+=1) {
            rs.push(accessor.call(context, array[i]));
        }
        return rs;
    };

    //accessor
    al.permutation = function(array, accessor, context, ascending) {
        var order = al.order(array, accessor, context, ascending);
        var permute = Array(array.length);
        order.forEach(function(d, i) {permute[d] = i});
        return permute;
    };

    al.order = function(array, accessor, context, ascending) {

        accessor = accessor || functools.identity;
        context = context || null;
//        ascending = ascending || true;

        var order = [];
        for (var i= 0, ii = array.length; i < ii; i++) {
            order.push(i);
        }

        order.sort(function(i, j) {
            return accessor.call(context, array[i]) - accessor.call(context, array[j]);
        });
        if (ascending == false)
            order.reverse();
        return order;
    };

    al.sort = function(array, accessor, context, ascending) {

        accessor = accessor || functools.identity;
        context = context || null;
        ascending = ascending || true;

        array.sort(function(a, b) {
            return accessor.call(context, a) - accessor.call(context, b);
        });
        if (ascending == false)
            array.reverse();
    };

    al.find_interval = function(val, array, accessor, context) {

        var n = array.length;

        if (n==0) { return -1;}
        if (val > accessor.call(context, array[n-1])) { return n; }
        if (val < accessor.call(context, array[0])) { return -1; }

        var p = 0,
            q = n-1;

        while(p <= q) {
            var m = Math.floor((p + q) / 2);
            var x = accessor.call(context, array[m]);
            if (val > x)
                p = m+1;
            else if (val < x)
                q = m-1;
            else
                return m;
        }
        return q;
    };

    al.interpolate_step = function(val, array, accessor, mapper, context) {

        context = context || null;

        var idx = algo.find_interval(val, array, accessor, context);

        if (idx == -1 || idx == array.length) {
            return null;
        }
        else {
            return mapper.call(context, array[idx]);
        }
    }

    al.find = function(val, array, accessor, context) {
        var idx = al.find_interval(val, array, accessor, context);
        if (idx == -1 || idx == array.length)
            return -1;
        else if (accessor.call(context, array[idx]) == val)
            return idx;
        else
            return -1;
    };

    al.find_element = function(val, array, accessor, context) {
        var idx = al.find(val, array, accessor, context);
        if (idx == -1)
            return null;
        else
            return array[idx];
    };

    al.argmin = function(array, accessor, context) {
        if (array.length == 0)
            return undefined;

        accessor = accessor || functools.identity;
        context = context || null;

        var min = accessor.call(context, array[0]),
            arg = array[0];

        for (var i=1, len=array.length; i < len; i++) {
            var val = accessor.call(context, array[i]);
            if (val < min) {
                min = val;
                arg = array[i];
            }
        }
        return arg;
    };

    al.argmax = function(array, accessor, context) {

         if (array.length == 0)
            return undefined;

        accessor = accessor || functools.identity;
        context = context || null;

        var max = accessor.call(context, array[0]),
            arg = array[0];

        for (var i= 1, len=array.length; i < len; i++) {
            var val = accessor.call(context, array[i]);
            if (val > max) {
                max = val;
                arg = array[i];
            }
        }
        return arg;
    };

    al.min = function(array, accessor, context) {

        context = context || null;
        accessor = accessor || functools.identity;

        var arg = al.argmin(array, accessor, context);
        if (arg == undefined)
            return arg;
        else
            return accessor.call(context, arg);
    };

    al.max = function(array, accessor, context) {

        context = context || null;
        accessor = accessor || functools.identity;

        var arg = al.argmax(array, accessor, context);
        if (arg == undefined)
            return arg;
        else
            return accessor.call(context, arg);
    };

    return al;

}();


EventDispatcher = function() {
    this.listeners = {};
};

var eproto = EventDispatcher.prototype;

eproto.on = function(event, f) {
    var ls = this.listeners;
    if (!ls.hasOwnProperty(event))
        ls[event] = [];
    ls[event].push(f);
    return this;
}

eproto.off = function(event, f) {
    //TODO: -low turn off listeners
    return this;
}

//pass object as args
eproto.fire = function(event, args) {
    var ls = this.listeners;
    if (!ls.hasOwnProperty(event))
        return;
    var fs = ls[event];
    for (var i= 0, ii=fs.length;i<ii;i+=1) {
        fs[i].call(this, args);
    }
}


/**
 * Created by user on 4/16/14.
 */
geom = function () {
    var g = {version: "0.0"};

    //please be careful of the applying orders
    g.transform = {
        value: '',
        begin: function() {
            this.value = '';
            return this;
        },
        end: function() {
            return this.value;
        },
        translate: function(dx, dy) {
            this.value += 'translate(' + dx + ',' + dy + ')';
            return this;
        },
        rotate: function(theta, x0, y0) {
            this.value += 'rotate(' + theta + ',' + x0 + ',' + y0 + ')';
            return this;
        },
        scale: function(fx, fy) {
            this.value += 'scale(' + fx + ',' + fy + ')';
            return this;
        }
    };

    /*
     get a path string by chaining functions
     example:
     g.path.begin() [.move_to(args), ...] .end()
    */
    g.path = {
        value:'',
        x:0,
        y:0,
        s: 0.5, //for curve easing
        begin: function(){
            this.value = '';
            return this;
        },
        move_to: function(x, y) {
            this.value += ' M ' + x + ' ' + y;
            this.x = x;
            this.y = y;
            return this;
        },
        line_to: function(x, y) {
            this.value += ' L ' + x + ' ' + y;
            this.x = x;
            this.y = y;
            return this;
        },
        eased_line_to: function(x, y) {
            var c0x = this.x,
                c0y = this.y,
                c1x = x,
                c1y = y;
            if ((x-this.x) * (y-this.y) > 0) {
                c0y = this.y * (1 - this.s) + y * this.s;
                c1x = this.x * this.s + x * (1 - this.s);
            }
            else {
                c0x = this.x * (1 - this.s) + x * this.s;
                c1y = this.y * this.s + y * (1 - this.s);
            }
            this.bezier_to(c0x, c0y, c1x, c1y, x, y);
            return this;
        },
        h_eased_line_to: function(x, y) {
            this.bezier_to(this.x * (1-this.s) + x * this.s, this.y, this.x * this.s + x * (1-this.s) , y, x, y);
            return this;
        },
        horizontal_to: function (x) {
            this.x = x;
            return this.line_to(x, this.y);
        },
        vertical_to: function(y) {
            this.y = y;
            return this.line_to(this.x, y);
        },
        horizontal_to_relative: function(x) {
            this.value += ' h ' + x;
            this.x = this.x + x;
            return this;
        },
        vertical_to_relative: function(y) {
            this.value += ' v ' + y;
            this.y = this.y + y;
            return this;
        },
        bezier_to: function(cx0, cy0, cx1, cy1, x1, y1) {
            this.x = x1;
            this.y = y1;
            this.value += ' C ' + cx0  + ',' + cy0 + ' ' + cx1 + ', ' + cy1 + ' ' + x1 + ', ' + y1;
            return this;
        },
        close_path: function() {
            this.value += ' Z ';
            return this;
        },
        end: function() {
            return this.value;
        }
    }

    g.scaling = function(width, height, objects, getter, setter) {

        var x_min = algo.min(objects, function(n) {return getter.call(null, n).x;}),
            x_max = algo.max(objects, function(n) {return getter.call(null, n).x;}),
            y_min = algo.min(objects, function(n) {return getter.call(null, n).y;}),
            y_max = algo.max(objects, function(n) {return getter.call(null, n).y;});

        var s = Math.min(width / (x_max - x_min), height / (y_max - y_min)),
            dx = (width - s * (x_max - x_min)) / 2,
            dy = (height - s * (y_max - y_min)) / 2;

        for (var i = 0, ii = objects.length; i < ii; i++) {
            setter.call(null,
                {x: ((getter.call(null, objects[i]).x - x_min) * s + dx),
                    y: ((getter.call(null, objects[i]).y - y_min) * s + dy)},
                objects[i]);
        }
    }

    return g;
}();

/**
 * Created by user on 4/16/14.
 */
matrix = function () {
    var m = {
        version: '0.0',
        consts: {
            ZERO: 1e-9
        },
        //initialize a zero matrix with m rows and n columns
        zeros : function (m, n) {
            var mat = [];
            for (var i= 0; i < m; i++) {
                var row = [];
                mat.push(row);
                for (var j = 0; j < n; j++) {
                    row.push(0);
                }
            }
            return mat;
        },
        identity: function(m) {
            var mat = this.zeros(m, m);
            for (var i = 0; i < m; i++) {
                mat[i][i] = 1;
            }
            return mat;
        },
        trace: function(mat) {
            var tr = [];
            for (var i = 0, ii = mat.length; i < ii; i++) {
                tr.push(mat[i][i]);
            }
            return tr;
        },
        col: function(j, mat) {
            var entries = [];
            for (var i = 0, ii = mat.length; i < ii; i ++) {
                entries.push(mat[i][j]);
            }
            return entries;
        },

        //return eigenvectors and eigenvalues
        //depend on science.lin.decompose routine in the package science.js
        //https://github.com/jasondavies/science.js/
        eigen: function(mat) {
            var decomposefunc = science.lin.decompose();
            var rs = decomposefunc.call(null, mat);
            var es = this.trace(rs.D)
                o = algo.order(es);
            var ev = [];
            for (var i = 0, ii = o.length; i < ii; i ++) {
                ev.push(this.col(o[i], rs.V));
            }
            algo.sort(es);
            //IDX is the first nonzero eigenvalue
            var i = 0;
            for (ii = es.length; i < ii; i ++) {
                if (Math.abs(es[i]) > this.consts.ZERO)
                    break;
            }
            return {E : es, V: ev, IDX: i};
        },
//      NOTE: we have to find rectangular area
        find_maximum_rectangle: function(mat) {
            //find rectangle with maximum area in the matrix
            var nrows = m.get_num_rows(mat),
                ncols = m.get_num_cols(mat),
                s = m.zeros(nrows, ncols),
                i0 = 0,
                j0 = 0,
                height = 0,
                width = 0;

            if (nrows == 0 || ncols == 0) return null;

            //histogram
            for (var i=0; i < nrows; i++) {
                var cnt = 0;
                for (var j=ncols-1; j >= 0; j--) {
                    if (mat[i][j] > 0)
                        cnt ++;
                    else
                        cnt = 0;
                    s[i][j] = cnt;
                }
            }

            var stack = [],
                area = 0;

            //maximum rectangle area in a histogram
            for (var j=0; j < ncols; j++) {
                var i = 0;
                while(i < nrows) {
                    if(stack.length == 0 || s[stack[stack.length-1]][j] <= s[i][j])
                        stack.push(i++);
                    else {
                        var tp = stack[stack.length-1];
                        stack.pop();
                        //stack is empty, all previous values larger than s[i][j]
                        //tp is the most previous one
                        if (stack.length == 0) {
                            if (i * s[tp][j] > area) {
                                area = i * s[tp][j];
                                i0 = tp;
                                j0 = j;
                                width = s[tp][j];
                                height = i;
                            }
                        }
                        else {
                            if ((i - stack[stack.length-1] -1) * s[tp][j] > area) {
                                area = (i - stack[stack.length-1] -1) * s[tp][j];
                                i0 = stack[stack.length-1] + 1;
                                j0 = j;
                                width = s[tp][j];
                                height = i - stack[stack.length-1] -1;
                            }
                        }
                    }
                }

                while (stack.length > 0) {
                   var tp = stack[stack.length-1];
                    stack.pop();
                    if (stack.length == 0) {
                        if (i * s[tp][j] > area) {
                            area = i * s[tp][j];
                            i0 = tp;
                            j0 = j;
                            width = s[tp][j];
                            height = i;
                        }
                    }
                    else {
                        if ((i - stack[stack.length-1] -1) * s[tp][j] > area) {
                            area = (i - stack[stack.length-1] -1) * s[tp][j];
                            i0 = stack[stack.length-1] + 1;
                            j0 = j;
                            width = s[tp][j];
                            height = i - stack[stack.length-1] -1;
                        }
                    }
                }
            }

            return {i0: i0, j0: j0, height: height, width: width};
        },
        clean_rectangle: function(i0, j0, height, width, mat) {
            for (var i=i0, ii=height + i0; i < ii; i++) {
                for (var j=j0, jj=width + j0; j < jj; j++) {
                    mat[i][j] = 0;
                }
            }
            return mat;
        },
        is_zero: function(mat) {
            var flag = true;
            for (var i= 0, ii= m.get_num_rows(mat); i < ii; i++) {
                for (var j= 0, jj= m.get_num_cols(mat); j < jj; j++) {
                    if (mat[i][j] != 0)
                        flag = false;
                }
            }
            return flag;
        },
        //find the nearest empty entry in the matrix with a spiral order
        find_nearest_empty_entry: function (i, j, mat) {
            if (i<0||i>=m.get_num_rows(mat)||j<0||j>=m.get_num_cols(mat)) {
                var ii = i,
                    jj = j;
                if (i < 0) ii = 0;
                if (i>= m.get_num_rows(mat)) ii = m.get_num_rows(mat) - 1;
                if (j < 0) jj = 0;
                if (j>= m.get_num_cols(mat)) jj = m.get_num_cols(mat) - 1;
                return {i: ii, j: jj};
            }
            var ii = i,
                jj = j,
                k = 0;
            while (mat[ii][jj] != 0) {
                if (ii == i - k && jj == j - k ) {
                    ii -= 1;
                    k += 1;
                }
                else if (ii == i - k && jj < j + k) {
                    jj += 1;
                }
                else if (jj == j + k && ii < i + k) {
                    ii += 1;
                }
                else if (ii == i + k && jj > j - k) {
                    jj -= 1;
                }
                else if (jj == j - k && ii > i - k) {
                    ii -= 1;
                }

                if (ii<0||ii>=m.get_num_rows(mat)||jj<0||jj>=m.get_num_cols(mat)) {
                    if (ii < 0) ii = 0;
                    if (ii>= m.get_num_rows(mat)) ii = m.get_num_rows(mat) - 1;
                    if (jj < 0) jj = 0;
                    if (jj>= m.get_num_cols(mat)) jj = m.get_num_cols(mat) - 1;
                    return {i: ii, j: jj};
                }

            }
            return {i: ii, j: jj};
        },
        get_num_rows: function(mat) {
            return mat.length;
        },
        get_num_cols: function(mat) {
            if (mat.length == 0)
                return 0;
            else
                return mat[0].length;
        },
        to_string: function(mat) {
            var str = '';
            var nrows = m.get_num_rows(mat),
                ncols = m.get_num_cols(mat);
        },
        //test spiral
        test_spiral: function() {
            var mat = matrix.zeros(7, 7);
            for (var i = 0, ii = 36; i < ii; i++) {
                var entry = m.find_nearest_empty_entry(3, 3, mat);
                mat[entry.i][entry.j] = 1;
                console.info(entry);
            }
        },
        //test finding maximum rectangle
        //passed simple test, should be ok
        test_max_rectangle: function() {
            var mat = matrix.zeros(8, 8);
            for (var i = 0, ii = 30; i < ii; i++) {
                var entry = m.find_nearest_empty_entry(3, 3, mat);
                mat[entry.i][entry.j] = 1;
            }
            console.info(mat);
            m.find_maximum_rectangle(mat);
        }
    };
    return m;
}();

/*
 created by panpan xpp2007@gmail.com
 */

Graph = function() {

    this.empty();

};

_.extend(Graph.prototype, {

    empty: function() {

        this.nodes = [];
        this.links = [];
        this.adjlinks = [];

        this.nodeIdx = {};
        this.linkIdx = {};

        this.invalidNodeIndices = [];
        this.nextNodeIdx = 0;
        this.invalidEdgeIndices = [];
        this.nextEdgeIdx = 0;

        return this;
    },

    addNode: function(nid, params) {

        //TODO: check if nid is already in

        var n = {
            id: nid + '',
            name: '',
            attrs: {}
        };

        var nidx = -1;

        if (this.invalidNodeIndices.length == 0) {
            this.nodes.push(undefined);
            this.adjlinks.push(undefined);
            nidx = this.nextNodeIdx;
            this.nextNodeIdx ++;
        } else {
            nidx = this.invalidNodeIndices.pop();
        }

        this.nodes[nidx] = n;
        this.adjlinks[nidx] = [];
        this.nodeIdx[nid] = nidx;
        this.updateNodeAttrs(params, nid);
        return this;
    },


    addLink: function(eid, source, target, params) {

        var e = {
            id: eid + '',
            source: source + '',
            target: target + '',
            attrs: {}
        };

        if (this.node(source) == undefined) {
            this.addNode(source);
        }
        if (this.node(target) == undefined) {
            this.addNode(target);
        }

        var eidx = -1;

        if (this.invalidEdgeIndices.length == 0) {
            this.links.push(null);
            eidx = this.nextEdgeIdx;
            this.nextEdgeIdx ++;
        } else {
            eidx = this.invalidEdgeIndices.pop();
        }

        var sidx = this.nodeIdx[source],
            tidx = this.nodeIdx[target];

        this.links[eidx] = e;
        this.linkIdx[eid] = eidx;

        this.adjlinks[sidx].push(eid);
        this.adjlinks[tidx].push(eid);

        this.updateLinkAttrs(params, eid);
        return this;
    },

    removeNode:  function(nid) {
        //remove links first
        for (var i= 0, adjs=this.adjacents(nid), ii = adjs.length; i< ii; i++) {
            this.removeLink(adjs[i]);
        }
        var nidx = this.nodeIdx[nid];
        this.invalidNodeIndices.push(nidx);
        this.nodes[nidx] = undefined;
        this.adjlinks[nidx] = [];
        return this;
    },

    removeLink: function(arg0, arg1) {
        var e = undefined,
            eidx = -1;
        if (arguments.length == 1) {
            e = this.link(arg0);
            eidx = this.linkIdx[arg0];
        }
        else if (arguments.length >= 2) {
            e = this.link(arg0, arg1);
            eidx = this.linkIdx[e.id];
        }
        this.invalidEdgeIndices.push(eidx);
        this.links[eidx] = undefined;
        //remove from adjacency list
        var s = this.adjlinks[this.nodeIdx[e.source]],
            t = this.adjlinks[this.nodeIdx[e.target]];

        s.splice(s.indexOf(e.id), 1);
        t.splice(t.indexOf(t.id), 1);

        return this;
    },

    node: function(nid) {
        return this.nodes[this.nodeIdx[nid]];
    },

    //overloaded function link
    link: function(arg0, arg1) {
        if (arguments.length == 1) //eid
            return this.links[this.linkIdx[arg0]];
        else if (arguments.length == 2) { //nid0, nid1
            var nlinks = this.adjacents(arg0),
                rs = [];
            for (var i= 0, ii = nlinks.length; i < ii; i ++) {
                if (this.link(nlinks[i]).source == arg0 && this.link(nlinks[i]).target == arg1
                    || this.link(nlinks[i]).source == arg1 && this.link(nlinks[i]).target == arg0) {
                    if(rs.indexOf(nlinks[i]) == -1)
                        rs.push(nlinks[i]);
                }
            }
            if (rs.length == 1) {
                return rs[0];
            }
            else if (rs.length == 0) {
                return null;
            }
            else {
                return rs; //for a multi-graph, return all the links between two nodes
            }
        }
        else
            return undefined;
    },

    neighbor: function(nid, eid) {
        if (nid == this.link(eid).source)
            return this.link(eid).target;
        else
            return this.link(eid).source;
    },

    adjacents: function(nid) {
        return this.adjlinks[this.nodeIdx[nid]];
    },

    neighbors: function(nid) {

        var eids = this.adjacents(nid),
            nbrs = [];

        for (var i= 0, ii=eids.length; i < ii; i ++) {
            var nbr = this.neighbor(nid, eids[i]);
            if (nbrs.indexOf(nbr) == -1)
                nbrs.push(nbr);
        }

        return nbrs;
    },

    degree: function(nid) {
        return this.adjacents(nid).length;
    },

    precedessors: function(nid) { },
    successors: function (nid) { },

    indegree: function(nid) { },
    outdegree: function(nid) { },


    getNodes: function() {
        var nids = [],
            invalids = this.invalidNodeIndices.sort();
        for (var i = 0, ii = this.nodes.length, j = 0; i < ii; i ++) {
            if (j < invalids.length && i < invalids[j] || j >= invalids.length) {
                nids.push(this.nodes[i].id);
            }
            else {
                j ++;
            }
        }
        return nids;
    },

    getLinks: function() {
        var eids = [],
            invalids = this.invalidEdgeIndices.sort();
        for (var i = 0, ii = this.links.length, j = 0; i < ii; i ++) {
            if (j < invalids.length && i < invalids[j] || j >= invalids.length) {
                eids.push(this.links[i].id);
            }
            else {
                j ++;
            }
        }
        return eids;
    },

    getNodeAttr: function(attr, nid) {
        return this.node(nid).attrs[attr];
    },

    getNodeAttrs: function(attrs, nid) {
        var rs = {};
        for (var i= 0, ii=attrs.length; i < ii; i ++) {
            rs[attrs[i]] = this.getNodeAttr(attrs[i], nid);
        }
        return rs;
    },

    getLinkAttr: function(attr, eid) {
        return this.link(eid).attrs[attr];
    },

    bfs: function(nid, dlimit) {

        var visited = d3.set([nid]),
            distances = d3.map(),
            queue = [nid];

        distances.set(nid, 0);

        while (queue.length > 0) {
            var n0 = queue.shift(),
                d = distances.get(n0);
            if (d >= dlimit)
                continue;
            var nbrs = this.neighbors(n0);
            for (var i=0, ii=nbrs.length; i<ii; i++) {
                if (!visited.has(nbrs[i])) {
                    queue.push(nbrs[i]);
                    visited.add(nbrs[i]);
                    distances.set(nbrs[i], d + 1);
                }
            }
        }

        return distances;
    },

    updateNodeAttrs: function(params, nid) {

        var n = this.node(nid);
        params = params || {};
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                switch (k) {
                    case 'id': break;
                    case 'name': n.name = params[k]; break;
                    default :n.attrs[k] = params[k]; break;
                }
            }
        }
    },

    updateLinkAttrs: function(params, eid) {

        var e = this.link(eid);
        params = params || {};
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                switch (k) {
                    case 'id': break;
                    case 'source': break;
                    case 'target': break;
                    default :e.attrs[k] = params[k]; break;
                }
            }
        }
    },
    //adjacency matrix based on the given order
    denseAdjacencyMatrix: function(nids) {

        var mat = matrix.zeros(nids.length, nids.length);

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i + 1, jj = nids.length; j < jj; j++) {
                var e = this.link(nids[i], nids[j]);
                if (_.isArray(e)) {
                    mat[j][i] = mat[i][j] = e.length;
                }
                else if (e != null){
                    mat[j][i] = mat[i][j] = 1;
                }
                else {
                }
            }
        }

        return mat;
    },

    laplacianMatrix: function(nids) {

        var mat = this.denseAdjacencyMatrix(nids),
            rowsum = [];

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var sum = 0.0;
            for (var j = 0, jj = nids.length; j < jj; j ++) {
                sum += mat[i][j];
            }
            rowsum.push(sum);
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            mat[i][i] = rowsum[i];
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = nids.length; j < jj; j ++) {
                mat[i][j] = - mat[i][j];
                mat[j][i] = - mat[j][i];
            }
        }

        return mat;
    },

    //-----------algorithm used in first paper submission---------------------
    bundles: function(nids) {

        var mat = this.denseAdjacencyMatrix(nids),
            bdls = [];

        //matrix
        for (var i = 0, ii = nids.length; i < ii; i++) {
            //fill in diagonal entries
            mat[i][i] = 1;
        }

        //detect rectangle areas and check for rectangles like in issue01.png
        while (!matrix.is_zero(mat)) {
            var rect = matrix.find_maximum_rectangle(mat);
            if (rect == null
                || (rect.height == 0 && rect.width == 0)
                || (rect.height == 1 && rect.width == 1))
                break;

            if (rect.height == 1 && rect.width > 1) {
                if (rect.i0 == rect.j0) {
                    rect.j0 += 1;
                    rect.width -= 1;
                }
                else {
                    if (rect.i0 == rect.j0 + rect.width - 1) {
                        rect.width -= 1;
                    }
                }
            }

            if (rect.width == 1 && rect.height > 1) {
                if (rect.i0 == rect.j0) {
                    rect.i0 += 1;
                    rect.height -= 1;
                }
                else {
                    if (rect.j0 == rect.i0 + rect.height - 1) {
                        rect.height -= 1;
                    }
                }
            }

            if (rect.height > rect.width) {
                bdls.push({
                    i0: rect.j0,
                    i1: rect.j0 + rect.width - 1,
                    j0: rect.i0,
                    j1: rect.i0 + rect.height - 1
                });
            }
            else {
                bdls.push({
                    i0: rect.i0,
                    i1: rect.i0 + rect.height - 1,
                    j0: rect.j0,
                    j1: rect.j0 + rect.width - 1
                });
            }

            matrix.clean_rectangle(rect.i0, rect.j0, rect.height, rect.width, mat);
            matrix.clean_rectangle(rect.j0, rect.i0, rect.width, rect.height, mat);
        }

        //check for size-one rectangles
        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = nids.length; j < jj; j++) {
                if (mat[i][j] != 0 && i != j) {
                    bdls.push({
                        i0: i,
                        i1: i,
                        j0: j,
                        j1: j
                    });
                }
            }
        }

        return bdls;
    },


    fuzzy_bundles: function(nids) {

        //TODO detect dense blocks in the adjacency matrix

    },

    info: function() {
        return '#nodes = ' + this.nodes.length + '; ' + '#links = ' + this.links.length;
    }

});
/**
 * Created by panpan on 6/8/14.
 */

var DynamicGraph = function() {

    Graph.call(this);

};

_.extend(DynamicGraph.prototype, Graph.prototype, {

    t0: function() {
        return algo.min(this.getLinks(), function(eid) {return this.getLinkAttr('t', eid);}, this);
    },

    t1: function() {
        return algo.max(this.getLinks(), function(eid) {return this.getLinkAttr('t', eid)}, this);
    },

    timerange: function(){
        return {t0: this.t0(), t1: this.t1()};
    },

    timestamps: function() {

        var t = this.t0(),
            links = this.getLinks(),
            o = algo.order(links, function (eid) {return this.getLinkAttr('t', eid);}, this);

        var ts = [t];

        for (var i = 0, ii = links.length; i < ii; i += 1) {
            var eid = links[o[i]],
                _t = this.getLinkAttr('t', eid);
            if (_t != t) {
                ts.push(_t);
                t = _t;
            }
        }

        return ts;
    },

    //TODO: use different methods
    //for large/small number of nodes
    getTemporalAggregatedSubGraph: function(nids, tbgn, tend) {

        var g = new Graph(),
            eids = [];

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = ii; j < jj; j ++) {
                var e = this.link(nids[i], nids[j]);
                if (_.isArray(e)) {
                    eids = eids.concat(e);
                }
                else {
                    if (e != null){
                        eids.push(e);
                    }
                }
            }
        }

        eids = _.filter(eids,
            function(eid) {
                var t = this.getLinkAttr('t', eid);
                return t >= tbgn && t <= tend;
            },
            this);

        for (var i = 0, ii = nids.length; i < ii; i++) {
            g.addNode(nids[i]);
        }

        for (var i = 0, ii = eids.length; i < ii; i++) {
            g.addLink(eids[i], this.link(eids[i]).source, this.link(eids[i]).target);
        }

        return g;
    },

    adjacentsAggregateByTime: function(nid) {

        var adjs = this.adjacents(nid),
            o = algo.order(adjs, function (eid) {
            return this.getLinkAttr('t', eid);
        }, this, true);

        var eidst = [],
            lastT = this.t0()-1,
            lastLinks = undefined;

        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            var e = this.link(adjs[o[i]]),
                t = this.getLinkAttr('t', adjs[o[i]]);
            if (t != lastT) {
                lastT = t;
                lastLinks = [];
                eidst.push({t: lastT, links: lastLinks});
            }
            lastLinks.push(e.id);
        }

        return eidst;
    },

    degreeByTime: function(nid) {

        var degrees = [],
            adjs = this.adjacentsAggregateByTime(nid);

        var degree = 0;
        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            degree += adjs[i].links.length;
            degrees.push({t: adjs[i].t, degree: degree});
        }

        return degrees;
    }
});

/* Add overloaded function */
functools.addMethod(DynamicGraph.prototype, 'adjacents',
    function(nid, tbgn, tend){

        var adjs = this.adjacents(nid);

        return _.filter(adjs,
            function (eid) {
                var t = this.getLinkAttr('t', eid);
                return t >= tbgn && t <= tend;
            },
            this);
    });

functools.addMethod(DynamicGraph.prototype, 'neighbors',
    function(nid, tbgn, tend){

        var adjs = this.adjacents(nid, tbgn, tend),
            rs = [];

        for (var i = 0, ii = adjs.length; i < ii; i ++) {
            rs.push(this.neighbor(nid, adjs[i]));
        }

        return rs;
    });




//node link graph
//author: panpan
//contact: xpp2007@gmail.com
NLGraph = function (graph, canvas, params) {

    var self = this;

    this.graph = graph;

    this.vis = {};

    this.vis.canvas = d3.select(element);

    this.vis.config = {
        width: $(element).width(),
        height: $(element).height(),
        node_size: 8,
        node_fill: '#ccc',
        node_fill_opacity: 0.8,
        node_stroke: '#fff',
        node_stroke_width: 2.0,
        link_stroke: '#9d9cc9',
        link_stroke_width: 1.0,
        link_stroke_opacity: 0.1
    };

    function display() {

        var canvas = self.vis.canvas;
        var g = self.graph;
        var c = self.vis.config;

        geom.scaling(c.width, c.height, g.getNodes(),
            function(nid){return g.getNodeAttrs(['x', 'y'], nid)},
            function(p, nid){ g.updateNodeAttrs(p, nid);});

        canvas.append('g').selectAll('.link')
            .data(g.getLinks())
            .enter()
            .append('path')
            .attr('class', 'link')
            .attr('d', function(eid){
                var src = g.link(eid).source,
                    tgt = g.link(eid).target;
                return geom.path.begin()
                    .move_to(g.getNodeAttr('x', src), g.getNodeAttr('y', src))
                    .line_to(g.getNodeAttr('x', tgt), g.getNodeAttr('y', tgt))
                    .end();
            })
            .attr('stroke', c.link_stroke)
            .attr('stroke-width', c.link_stroke_width)
            .attr('stroke-opacity', c.link_stroke_opacity)
            .attr('fill-opacity', 0);

        canvas.append("g").selectAll(".node")
            .data(g.getNodes())
            .enter()
            .append('circle')
            .attr('class', 'node')
            .attr('cx', function (nid) {
                return g.getNodeAttr('x', nid);
            })
            .attr('cy', function (nid) {
                return g.getNodeAttr('y', nid);
            })
            .attr('r', function (nid) {
                return g.degree(nid);
            })
            .attr('fill', c.node_fill)
            .attr('fill-opacity', c.node_fill_opacity)
            .attr('stroke', c.node_stroke)
            .attr('stroke-width', c.node_stroke_width);

    }


    function clean() {
        //TODO: clean stuffs on canvas
    };

    this.display = display;
    this.clean = clean;

};

/**
 * some adaptions made
 */

/**
 * optimal dendrogram ordering
 *
 * implementation of binary tree ordering described in [Bar-Joseph et al., 2003]
 * by Renaud Blanch.
 * JavaScript translation by Jean-Daniel Fekete.
 *
 * [Bar-Joseph et al., 2003]
 * K-ary Clustering with Optimal Leaf Ordering for Gene Expression Data.
 * Ziv Bar-Joseph, Erik D. Demaine, David K. Gifford, Angèle M. Hamel,
 * Tommy S. Jaakkola and Nathan Srebro
 * Bioinformatics, 19(9), pp 1070-8, 2003
 * http://www.cs.cmu.edu/~zivbj/compBio/k-aryBio.pdf
 */

var leafOrder = function (_distFunc, _isLeaf, _left, _right) {

    var leavesMap = {},
        orderMap = {},
        distFunc = _distFunc,
        isLeaf = _isLeaf,
        left = _left,
        right = _right;

    function leaves(n) {
        if (n == null) return [];
        if (n.id in leavesMap)
            return leavesMap[n.id];
        return (leavesMap[n.id] = _leaves(n));
    }

    function _leaves(n) {
        if (n == null) return [];
        if (isLeaf(n)) return [n.id];
        return leaves(left(n)).concat(leaves(right(n)));
    }

    function order(v, i, j) {
        var key = "k" + v.id + "-" + i + "-" + j; // ugly key
        if (key in orderMap)
            return orderMap[key];
        return (orderMap[key] = _order(v, i, j));
    }

    function _order(v, i, j) {

        if (isLeaf(v))
            return [0, [v.id]];

        var l = left(v), r = right(v);
        var L = leaves(l), R = leaves(r);

        var w, x;
        if (L.indexOf(i) != -1 && R.indexOf(j) != -1) {
            w = l;
            x = r;
        }
        else if (R.indexOf(i) != -1 && L.indexOf(j) != -1) {
            w = r;
            x = l;
        }
        else
            throw {
                error: "Node is not common ancestor of " + i + ", " + j
            };

        var Wl = leaves(left(w)), Wr = leaves(right(w));
        var Ks = Wr.indexOf(i) != -1 ? Wl : Wr;
        if (Ks.length == 0)
            Ks = [i];

        var Xl = leaves(left(x)), Xr = leaves(right(x));
        var Ls = Xr.indexOf(j) != -1 ? Xl : Xr;
        if (Ls.length == 0)
            Ls = [j];

        var max = 0, optimal_order = [];

        for (var k = 0; k < Ks.length; k++) {

            var w_max = order(w, i, Ks[k]);
            for (var l = 0; l < Ls.length; l++) {
                var x_max = order(x, Ls[l], j);

                var sim = w_max[0] + distFunc(Ks[k], Ls[l]) + x_max[0];
                if (sim > max) {
                    max = sim;
                    optimal_order = w_max[1].concat(x_max[1]);
                }
            }
        }
        return [max, optimal_order];
    }

    function orderFull(v) {

        var max = 0,
            optimal_order = [],
            leftLeaves = leaves(left(v)),
            rightLeaves = leaves(right(v));

        for (var i = 0; i < leftLeaves.length; i++) {
            for (var j = 0; j < rightLeaves.length; j++) {

                var so = order(v, leftLeaves[i], rightLeaves[j]);
                if (so[0] > max) {
                    max = so[0];
                    optimal_order = so[1];
                }

            }
        }

        return optimal_order;
    };

    function reorder(root) {
        return orderFull(root);
    };

    return reorder;
};
/**
 * Created by user on 6/12/14.
 */

var config = {

    visual: {

        overview: {

            dftLeftPadding: 100,
            dftTopPadding: 160,
            dftMaxLog2Degree: 8,
            dftMaxDistance: 5,
            dftStepSize: 50,
            dftPixelSize: 0.60,
            dftFillColor: '#31a354',
            dftQuantizeLevel: 25,
            dftBarWidth: 60,
            dftBarGap: 10

        },

        timeline: {

            dftHeight: 600,
            dftWidth: 20000,

            //parameter for general layout
            dftTopPadding: 80,
            dftLeftPadding: 180,

            //maximum #nodes displayed in detail
            dftNodeLimit: 50,
            dftNodeHalfLimit: 50,

            //parameter for vertical space occupied by each node
            dftLineHeight: 10,
            dftLineDistance: 2.0,

            //parameter for horizontal arrangement to display temporal information
            dftTimeStepPadding: 1.0,

            dftSeamWidth: 0.2,
            dftSeamStrokeColor: '#b4b8b3',
            dftSeamStrokeOpacity: 1.0,

            dftLeadYBuffer: 3.0,
            dftLeadStrokeWidth: 0.8,
            dftLeadStrokeOpacity: 0.8,

            //parameter for displaying snapshot graphs
            dftSnapShotWidth: 160,
            dftSnapShotPadding: 6,
            dftSnapShotLinkColor: '#b4b8d3',
            dftSnapShotLinkOpacity: 0.5,


            //parameter for displaying nodes
            dftNodeStrokeColor: '#fff',
            dftNodeFillColor: '#009393',
            dftNodeFillOpacity: 0.08,
            dftNodeHighlightFillColor: '#acdb04',
            dftNodeHighLightStrokeColor: '#ffffff',
            dftNodeStrokeOpacity: 0.25,
            dftNodeTip: undefined,

            //parameter for displaying glyphs for each edge deletion & creation events
            dftGlyphMaxLength: 8,
            dftGlyphMinLength: 2,
            dftGlyphMaxRotate: 45,
            dftGlyphWidth: 2.5,
            dftGlyphPadding: 0.0,

            dftGlyphStrokeWidth: 0.7,
            dftGlyphStrokeColor: '#222',
            dftGlyphStrokeOpacity: 1.0,
            dftGlyphStrokeFadedOpacity: 0.5,

            dftLabelFontSize: '0.65rem',
            dftLabelFontColor: '#999',
            dftLabelHighLightFontColor: '#e0605c',
            dftLabelFontWeight: 400,
            dftLabelHighlightFontWeight: 600,
            dftLabelPadding: 24,
            dftTimeStampFontHeight: 16,
            dftFontFamily: 'Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif',

            dftTransitionDuration: 1500,

            dftDOI: function() {
                var dftDOIWeights = [600.0, 1.0],
                    dftDOIFunc = [function(d) {return 1.0 / ( d + 1 )}, functools.identity];
                var doi = 0.0;
                for (var i = 0, ii = arguments.length; i < ii; i ++) {
                    doi += dftDOIWeights[i] * dftDOIFunc[i].call(null, arguments[i]);
                }
                return doi;
            },

            dftDegreeQuantizerMax: 8,

            dftDegreeQuantizer: function(d) {
                if (d == 0)
                    return 0;
                var sign = Math.abs(d) / d,
                    d = Math.abs(d),
                    x = Math.ceil(Math.log(d) / Math.log(2));
                x = x > config.visual.timeline.dftDistanceQuantizerMax ? config.visual.timeline.dftDistanceQuantizerMax : x;
                return x * sign;
            },

            dftDistanceQuantizerMax: 5,

            dftDistanceQuantizer: function(d) {
                if (d == -1) {
                    return config.visual.timeline.dftDistanceQuantizerMax;
                }

                if (d <= config.visual.timeline.dftDistanceQuantizerMax - 1)
                    return d;
                else
                    return config.visual.timeline.dftDistanceQuantizerMax;
            },

            dftGlyphLengthScalingFunc: function(x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([1, config.visual.timeline.dftDistanceQuantizerMax])
                    .range([config.visual.timeline.dftGlyphMinLength, config.visual.timeline.dftGlyphMaxLength]);

                return f0(d);

            }

        },

        small_multiples: {

            dftWidth: 1250,
            dftHeight: 750,

            dftLeftPadding: 50,
            dftTopPadding: 50,

            dftHGap: 25,
            dftVGap: 25,

            dftBlockWidth: 200,
            dftBlockHeight: 200,

            dftMaxNodeSize: 8,
            dftMinNodeSize: 1,

            dftNodeFillColor: '#ffffff',

            dftMaxLinkWidth: 4,
            dftMinLinkWidth: 1,


            dftLinkStrokeColor: '#333333',
            dftLinkStrokeHighLightColor: '#d5bc9c',

            dftMaxIteration: 500,

            dftTransitionDuration: 400,

            dftNodeSizeScalingFunc: function(x) {

                var d = config.visual.timeline.dftDegreeQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([0, config.visual.timeline.dftDegreeQuantizerMax])
                    .range([config.visual.small_multiples.dftMinNodeSize, config.visual.small_multiples.dftMaxNodeSize]);

                return f0(d);
            },

            dftLinkWidthScalingFunc: function(x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain([0, config.visual.timeline.dftDistanceQuantizerMax])
                    .range([config.visual.small_multiples.dftMinLinkWidth, config.visual.small_multiples.dftMaxLinkWidth]);

                return f0(d);
            },

            dftLinkColorFunc: function (x) {

                var d = config.visual.timeline.dftDistanceQuantizer(x);
                var f0 = d3.scale.linear()
                    .domain(d3.range(0, config.visual.timeline.dftDistanceQuantizerMax))
                    .range(colorbrewer.YlOrRd[8].slice(4, 7).reverse());

                return f0(d);
            }

        },

        hierarchy: {

            dftWidth: 1000,
            dftHeight: 750,

            dftDendroWidth: 240,
            dftDendroHeight: 700,

            dftTimeStepWidth: 20,

            dftLeftPadding: 50,
            dftTopPadding: 50,

            dftFillColor: '#ed7a3c',
            dftStrokeColor: '#ed7a3c'

        },

        link_diagram: {

            width: 160,
            linkColor: '#b4b8d3',
            linkOpacity: 0.6,
            fadeInDuration: 1000

        }
    }

}

/**
 * Created by panpan on 6/12/14.
 */

var Timeline = function(graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.state = {
        visual: {
            focuses:  [],
            snapshots: [],
            visible_nodes: [],
            order: {}   //order of the nodes displayed at full
        },
        cache: {
            node_attributes: {} // in the form of {nid: {attr0: val0, attr1: val1}}
        }
    };

    self.constants = {
        visual: {
            hidden: 0,
            half: 1,
            full: 2
        },
        event: {
            refocus: 'refocus',
            opensnap: 'opensnap',
            shuffle: 'shuffle'
        }
    };

    //append visual attributes
    function init() {

        var g = self.graph,
            nids = g.getNodes(),
            eids = g.getLinks(),
            consts = self.constants;

        for (var i = 0, ii = nids.length; i < ii; i++) {
            g.updateNodeAttrs({
                visual: {
                    doi: 0,
                    x: 0,
                    y: 0,
                    xt: [],
                    state: consts.visual.hidden,
                    _x: 0,
                    _y: 0,
                    _xt: [],
                    _state: consts.visual.hidden
                }
            }, nids[i]);
        }

        for (var i = 0, ii = eids.length; i < ii; i++) {
            g.updateLinkAttrs({
                    visual: {
                        x: 0,
                        y: 0,
                        length: 0,
                        angle: 0
                    }
                }, eids[i]);
        }

        registerEvents();

    }


    function updateDOI() {

        var depth = 2,
            graph = self.graph,
            nids = self.graph.getNodes(),
            roots = self.state.visual.focuses,
            config = self.config,
            dists = d3.map();

        for (var i = 0, ii = roots.length; i < ii; i++) {
            var _dists = graph.bfs(roots[i], depth);
            _dists.forEach(function (k, v) {
                if (!dists.has(k)) dists.set(k, v);
                else dists.set(k, Math.min(dists.get(k), _dists.get(k)));
            });
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var n = graph.node(nids[i]);
            n.attrs.visual.doi = graph.degree(nids[i]);
            if (dists.has(n.id)) {
                n.attrs.visual.doi = config.dftDOI.call(null, dists.get(n.id), graph.degree(nids[i]));
            }
        }
    };

    function updateVisualState() {

        var graph = self.graph,
            nids = graph.getNodes(),
            consts = self.constants,
            state = self.state,
            config = self.config;

        var p = algo.permutation(nids, function (nid) {
            return graph.node(nid).attrs.visual.doi;
        }, null, false);

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            if (p[i] < config.dftNodeLimit)
                graph.node(nids[i]).attrs.visual.state = consts.visual.full;
            if (p[i] >= config.dftNodeHalfLimit)
                graph.node(nids[i]).attrs.visual.state = consts.visual.hidden;
        };

        state.visual.visible_nodes = graph.getNodes().filter(function(nid) {return graph.node(nid).attrs.visual.state != consts.visual.hidden});

    };

    //some attributes used in visualization, will not change upon focus change
    function updateDataCache() {

        var graph = self.graph,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        cache.node_attributes = {}; //clear

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var nbrs = graph.neighbors(nids[i]);
            cache.node_attributes[nids[i]] = {};
            for (var j = 0, jj = nbrs.length; j < jj; j++) {
                if (cache.node_attributes[nbrs[j]] == undefined) {
                    cache.node_attributes[nbrs[j]] = {};
                }
            }
        }


        for (var i = 0, ii = nids.length; i < ii; i++) {
            var nbrs = graph.neighbors(nids[i]);
            cache.node_attributes[nids[i]]['t_degree'] = graph.degreeByTime(nids[i]);
            for (var j = 0, jj = nbrs.length; j < jj; j ++) {
                if (cache.node_attributes[nbrs[j]]['t_degree'] == undefined) {
                    cache.node_attributes[nbrs[j]]['t_degree'] = graph.degreeByTime(nbrs[j]);
                }
            }
        }
    }

    function updateLayout() {

        var graph = self.graph,
            state = self.state,
            nids = state.visual.visible_nodes;

        var ag = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), graph.t1()),
            lpm = ag.laplacianMatrix(nids),
            ev = matrix.eigen(lpm);

        var permutation = algo.permutation(ev.V[ev.IDX]);

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            state.visual.order[nids[i]] = permutation[i];
        }
    }

    function updateVerticalPosition() {

        var graph = self.graph,
            config = self.config,
            state = self.state,
            nids = state.visual.visible_nodes,
            p = state.visual.order; //node permutation

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            graph.node(nids[i]).attrs.visual.y = p[nids[i]] * (config.dftLineHeight + config.dftLineDistance);
        }

    }

    function updateHorizontalPosition() {

        var graph = self.graph,
            config = self.config,
            state = self.state,
            nodes = state.visual.visible_nodes,
            timestamps = graph.timestamps();

        //update horizontal layouts
        //get links at each timestamp and initialize x position
        var nvlinks = [],
            nvx = [];
        for (var i= 0,ii=nodes.length; i<ii; i++) {
            nvlinks.push(graph.adjacentsAggregateByTime(nodes[i]));
            nvx.push(config.dftLeftPadding);
        }

        //clean positioning
        for (var i= 0, ii=nodes.length; i<ii; i++)
            graph.node(nodes[i]).attrs.visual.xt = [];

        //x positioning
        for (var k = 0, kk = timestamps.length; k < kk; k ++) {
            var t = timestamps[k];
            for (var i = 0, ii = nodes.length; i<ii; i++) {
                //assign current positions
                graph.node(nodes[i]).attrs.visual.xt.push({t: t, x: nvx[i]});
                //increment current position
                var el = algo.find_element(t, nvlinks[i], function(d) {return d.t;});
                if (el != null)
                    nvx[i] += el.links.length * config.dftGlyphWidth;

                nvx[i] += config.dftTimeStepPadding * 2;
                nvx[i] += config.dftSeamWidth;
            }

            //leave space for a snapshot
            if (state.visual.snapshots.indexOf(t) > -1) {
                var mx = config.dftSnapShotWidth + 2 * config.dftSnapShotPadding + algo.max(nvx);
                for (var i = 0, ii = nvx.length; i<ii; i++)
                    nvx[i] = mx;
            }

        }
    }

    function renderNodes() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            consts = self.constants,
            state = self.state,
            nids = state.visual.visible_nodes;

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.node')
            .data(nids)
            .enter()
            .append('g')
            .attr('class', 'node')
            .attr('transform', function(nid) {
                return geom.transform.begin().translate(0, graph.node(nid).attrs.visual.y).end();
            })
            .append('rect')
            .attr('class', 'node_substrate')
            .attr('width', config.dftWidth).attr('height', config.dftLineHeight)
            .classed('selected', function(nid) {
               return state.visual.focuses.indexOf(nid) > -1;
            });

        canvas.selectAll('.node')
            .append('text')
            .attr('class', 'node_label')
            .attr('x', config.dftLabelPadding)
            .attr('y', config.dftLineHeight)
            .attr('font-size', config.dftLabelFontSize)
            .attr('color', config.dftLabelFontColor)
            .style('font-family', config.dftFontFamily)
            .text(function(nid) {return graph.node(nid).name;});

    }

    function renderLinks() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            state = self.state,
            nids = state.visual.visible_nodes,
            cache = state.cache;

        canvas.selectAll('.node')
            .each(function (nid) {

                var n = graph.node(nid),
                    adjs = graph.adjacentsAggregateByTime(nid),
                    ts = algo.listgen(adjs, function(el) { return el.t;});

                for (var i = 0, ii = adjs.length; i < ii; i++) {
                    var t = adjs[i].t;
                    adjs[i].links.forEach(function (eid) {
                        var e = graph.link(eid),
                            n0id = nid,
                            n1id = graph.neighbor(nid, eid);
                        var d0 = algo.interpolate_step(t - 1, cache.node_attributes[n0id]['t_degree'], function(el) {return el.t;}, function(el) {return el.degree;}) || 0;
                        var d1 = algo.interpolate_step(t - 1, cache.node_attributes[n1id]['t_degree'], function(el) {return el.t;}, function(el) {return el.degree;}) || 0;

                        e.attrs.visual.length = config.dftGlyphLengthScalingFunc(e.attrs['dist']);

                        e.attrs.visual.angle = config.dftDegreeQuantizer(d0 - d1) / config.dftDegreeQuantizerMax * config.dftGlyphMaxRotate;
                    });
                }

                d3.select(this)
                    .selectAll('.step')
                    .data(ts)
                    .enter()
                    .append('g')
                    .attr('class', 'step')
                    .attr('transform', function(t) {
                        var x = config.dftTimeStepPadding + algo.interpolate_step(t, n.attrs.visual.xt, function(el) {return el.t;}, function(el) {return el.x;}),
                            y = 0;
                        return geom.transform.begin().translate(x, y).end();
                    })
                    .append('rect')
                    .attr('class', 'step_substrate')
                    .attr('x', 0).attr('y', 0)
                    .attr('width', function(t, i) {
                        return adjs[i].links.length * config.dftGlyphWidth;
                    })
                    .attr('height', config.dftLineHeight)


                d3.select(this)
                    .selectAll('.step')
                    .append('g')
                    .each(function(t, i) {

                        var eids = adjs[i].links,
                            x0 = 0,
                            p = algo.permutation(eids, function(eid) {return graph.link(eid).attrs.visual.angle;});

                        for (var i = 0, ii = eids.length; i < ii; i++) {
                            var e = graph.link(eids[i]);
                            e.attrs.visual.x = x0 + p[i] * config.dftGlyphWidth + config.dftGlyphWidth / 2;
                        }

                        d3.select(this).selectAll('.link_glyph')
                            .data(eids)
                            .enter()
                            .append('g')
                            .attr('class', 'link_glyph')
                            .attr('transform', function (eid) {
                                var e = graph.link(eid);
                                return geom.transform.begin()
                                    .translate(e.attrs.visual.x, 0)
                                    .rotate(e.attrs.visual.angle, 0, config.dftLineHeight / 2)
                                    .end();
                            })
                            .append('path')
                            .attr('class', 'component0')
                            .attr('d', function () {
                                return geom.path.begin()
                                    .move_to(0, (config.dftLineHeight - config.dftGlyphMaxLength) / 2)
                                    .vertical_to_relative(config.dftGlyphMaxLength)
                                    .end();
                            });

                        d3.select(this).selectAll('.link_glyph')
                            .append('path')
                            .attr('class', 'component1')
                            .attr('d', function(eid) {
                                var e = graph.link(eid);
                                return geom.path.begin()
                                    .move_to(0, (config.dftLineHeight - e.attrs.visual.length) / 2)
                                    .vertical_to_relative(e.attrs.visual.length)
                                    .end();
                            });
                    });
            });
    }

    //update horizontal position of the links
    function updateLinksHPosition() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config;

         canvas.selectAll('.node')
            .each(function (nid) {
                var n = graph.node(nid);
                d3.select(this)
                    .selectAll('.step')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('transform', function(t) {
                        var x = config.dftTimeStepPadding + algo.interpolate_step(t, n.attrs.visual.xt, function(el) {return el.t;}, function(el) {return el.x;}),
                            y = 0;
                        return geom.transform.begin().translate(x, y).end();
                    });
             });
    }


    function renderSnapshots() {

        var graph = self.graph,
            canvas = self.canvas,
            config = self.config,
            state = self.state,
            snapshots = state.visual.snapshots,
            nids = state.visual.visible_nodes;

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .attr('class', 'snapshots')
            .selectAll('.snapshot')
            .data(snapshots)
            .enter()
            .append('g')
            .attr('class', 'snapshot')
            .attr('transform', function(t) {
                return geom.transform.begin()
                    .translate(algo.interpolate_step(t+1, graph.node(nids[0]).attrs.visual.xt, function(el){return el.t;}, function(el){return el.x;}) - config.dftSnapShotWidth -config.dftSnapShotPadding, 0)
                    .end();
            });

        //clean space
        canvas.selectAll('.snapshot')
            .append('rect')
            .attr('x', 0)
            .attr('y', config.dftTopPadding)
            .attr('width', config.dftSnapShotWidth)
            .attr('height', config.dftHeight)
            .attr('fill', '#fff');

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.selectAll('.snapshot')
            .each(function (t) {

                var g = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), t),
                    bdls = g.bundles(nids);

                d3.select(this).selectAll('.bundle')
                    .data(d3.range(0, bdls.length))
                    .enter()
                    .append('g')
                    .attr('class', 'bundle')
                    .each(function(i) {

                        //multiple edges in a bundle
                        if (bdls[i].i0 != bdls[i].i1 || bdls[i].j0 != bdls[i].j1) {

                            d3.select(this)
                                .append('path')
                                .attr('class', 'multiple')
                                .attr('d', function (i) {

                                    var bdl = bdls[i],
                                        ni0 = graph.node(nids[bdl.i0]),
                                        ni1 = graph.node(nids[bdl.i1]),
                                        nj0 = graph.node(nids[bdl.j0]),
                                        nj1 = graph.node(nids[bdl.j1]),
                                        path = geom.path.begin(),
                                        x = config.dftSnapShotWidth;

                                    if (bdls[i].i0 == bdls[i].i1) {

                                        path.move_to(0, ni0.attrs.visual.y + config.dftLineHeight / 2)
                                            .h_eased_line_to(x, nj0.attrs.visual.y)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight / 2)
                                            .close_path();

                                    }
                                    else if (bdls[i].j0 == bdls[i].j1) {

                                        path.move_to(0, ni0.attrs.visual.y)
                                            .h_eased_line_to(x, nj0.attrs.visual.y + config.dftLineHeight / 2)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight / 2)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight)
                                            .close_path();

                                    }
                                    else {

                                        path.move_to(0, ni0.attrs.visual.y)
                                            .h_eased_line_to(x, nj0.attrs.visual.y)
                                            .line_to(x, nj1.attrs.visual.y + config.dftLineHeight)
                                            .h_eased_line_to(0, ni1.attrs.visual.y + config.dftLineHeight)
                                            .close_path();

                                    }

                                    return path.end();
                                })
                                .attr('fill', config.dftSnapShotLinkColor)
                                .attr('stroke-opacity', 0.0)
                                .style('opacity', 0)
                                .transition()
                                .duration(config.dftTransitionDuration)
                                .style('opacity', config.dftSnapShotLinkOpacity);

                        }
                        else {

                            d3.select(this)
                                .append('path')
                                .attr('class', 'single')
                                .attr('transform', geom.transform.begin().translate(0, config.dftLineHeight / 2).end())
                                .attr('d', function (i) {

                                    var bdl = bdls[i],
                                        ni0 = graph.node(nids[bdl.i0]),
                                        nj0 = graph.node(nids[bdl.j0]),
                                        path = geom.path.begin(),
                                        x = config.dftSnapShotWidth;

                                    path.move_to(0, ni0.attrs.visual.y)
                                        .h_eased_line_to(x, nj0.attrs.visual.y);

                                    return path.end();
                                })
                                .attr('stroke', config.dftSnapShotLinkColor)
                                .attr('stroke-opacity', 0.0)
                                .transition()
                                .duration(config.dftTransitionDuration)
                                .style('stroke-opacity', config.dftSnapShotLinkOpacity);

                        }
                    });
            })

    }

    function cleanSnapShots() {
        d3.selectAll('.snapshots').remove();
    }

    function renderTimestamps() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(0, config.dftTopPadding).end())
            .selectAll('.timestamp')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'timestamp')
            .append('path')
            .attr('class', 'seam')
            .attr('d', function(t) {

                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x}),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i< ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function(el) {return el.t;}, function(el) {return el.x;});

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);

                }

                path.line_to(x, y + config.dftLineHeight);
                return path.end();
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .append('text')
                    .attr('class', 'timestamp_label')
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end())
                    .text(t);

                d3.select(this)
                    .append('path')
                    .attr('class', 'lead')
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );

            });
    }

    function updateTimestampsHPosition() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            state = self.state,
            nids = state.visual.visible_nodes,
            ts = graph.timestamps();

        algo.sort(nids, function(nid) {return state.visual.order[nid];});

        canvas.selectAll('.timestamp')
            .each(function(t) {
                var x = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x
                        }),
                    y = graph.node(nids[0]).attrs.visual.y,
                    path = geom.path.begin().move_to(x, y);

                for (var i = 1, ii = nids.length; i < ii; i++) {
                    y += config.dftLineHeight;
                    path.line_to(x, y);
                    x = algo.interpolate_step(t, graph.node(nids[i]).attrs.visual.xt,
                        function (el) {
                            return el.t;
                        }, function (el) {
                            return el.x;
                        });

                    y = graph.node(nids[i]).attrs.visual.y;
                    path.vertical_to_relative(config.dftLineDistance / 2.0);
                    path.horizontal_to(x);
                    path.line_to(x, y);
                }

                path.line_to(x, y + config.dftLineHeight);

                path = path.end();

                d3.select(this)
                    .select('.seam')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', path);
            });

        //positions for timestamp labels
        var xs = [],
            x0 = config.dftLeftPadding;

        for (var i = 0, ii = ts.length; i < ii; i++) {
            var x = algo.interpolate_step(ts[i], graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function (el) {return el.x;});
            if (x > x0) {
                xs.push(x);
                x0 = x + config.dftTimeStampFontHeight;
            } else {
                xs.push(x0);
                x0 += config.dftTimeStampFontHeight;
            }
        }

        canvas.selectAll('.timestamp')
            .each(function(t, i) {

                var x = xs[i],
                    y = - config.dftLabelPadding,
                    xx = algo.interpolate_step(t, graph.node(nids[0]).attrs.visual.xt, function (el) {return el.t;}, function(el) {return el.x;}),
                    yy = 0;

                d3.select(this)
                    .select('.timestamp_label')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('x', x).attr('y', y)
                    .attr('transform', geom.transform.begin().rotate(-90, x, y).end());

                d3.select(this)
                    .select('.lead')
                    .transition()
                    .duration(config.dftTransitionDuration)
                    .attr('d', geom.path.begin()
                        .move_to(x, y)
                        .vertical_to(y + config.dftLeadYBuffer)
                        .line_to(xx, yy - config.dftLeadYBuffer)
                        .vertical_to(yy)
                        .end()
                    );
            });

    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    function render() {
        renderNodes();
        renderLinks();
        renderSnapshots();
        renderTimestamps();
    }

    function refocus() {
        updateDOI();
        updateVisualState();
        updateDataCache();
    }

    function layout() {
        updateLayout();
        updateVerticalPosition();
        updateHorizontalPosition();
    }

    function setTimestampStatus(t, status, flag, context) {
        d3.select(context).selectAll('.timestamp_label,.seam, .lead').classed(status, flag);
        self.canvas.selectAll('.node').selectAll('.step').filter(function (_t) {
            return _t == t;
        }).selectAll('.step_substrate').classed(status, flag);
    }

    function toggleTimestampStatus(t, status, context) {
//        console.info(d3.select(context).selectAll('.timestamp_label'));
        if (d3.select(context).selectAll('.timestamp_label').classed(status)) {
            setTimestampStatus(t, status, false, context);
        }
        else {
            setTimestampStatus(t, status, true, context);
        }
    }

//    function setNodeStatus(t)

    function registerCallbacks() {

        var snapshots = self.state.visual.snapshots,
            event = self.constants.event,
            state = self.state,
            config = self.config;

//        self.canvas.selectAll('.node').select('.node_substrate')
//            .on('mouseover', function () {
//                d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
//            })
//            .on('mouseout', function (nid) {
//                var focuses = state.visual.focuses;
//                if (focuses.indexOf(nid) == -1) {
//                    d3.select(this).attr('fill', '#fff');
//                }
//            })
//            .on('dblclick', null)
//            .on('dblclick', function (nid) {
//                var focuses = state.visual.focuses;
//                if (focuses.indexOf(nid) > -1) {
//                    d3.select(this).attr('fill', '#fff');
//                    focuses.splice(focuses.indexOf(nid), 1);
//                }
//                else {
//                    d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
//                    focuses.push(nid);
//                }
//                self.eventDispatcher.fire(event.refocus);
//            });

        //hovering & click highlights
        self.canvas.selectAll('.node').select('.node_substrate')
            .on('mouseover', function() {
                d3.select(this).classed('highlight', true);
            })
            .on('mouseout', function() {
                d3.select(this).classed('highlight', false);
            })
            .on('click', function() {
                if (d3.select(this).classed('focus')){
                    d3.select(this).classed('focus', false);
                }
                else {
                    d3.select(this).classed('focus', true);
                }
            })
            .on('dblclick', function(nid) {
                if (d3.select(this).classed('selected')) {
                    d3.select(this).classed('selected', false);
                }
                else {
                    d3.select(this).classed('selected', true);
                }

                var focuses = state.visual.focuses;
                if (focuses.indexOf(nid) > -1) {
                    d3.select(this).attr('fill', '#fff');
                    focuses.splice(focuses.indexOf(nid), 1);
                }
                else {
//                    d3.select(this).attr('fill', config.dftNodeHighlightFillColor);
                    focuses.push(nid);
                }
                self.eventDispatcher.fire(event.refocus);

            });


        self.canvas.selectAll('.timestamp').select('.timestamp_label')
            .on('dblclick', null)
            .on('dblclick', function (t) {
                var idx = snapshots.indexOf(t);
                if (idx > -1) {
                    snapshots.splice(idx, 1); snapshots.sort();
                }
                else {
                    snapshots.push(t); snapshots.sort();
                }
                toggleTimestampStatus(t, 'focus', this.parentNode);
                self.eventDispatcher.fire(event.opensnap);
            });

        //hovering & click highlights
        self.canvas.selectAll('.timestamp').selectAll('.timestamp_label, .seam, .lead')
            .on('mouseover', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', true, this.parentNode);
            })
            .on('mouseout', function() {
                var t = d3.select(this.parentNode).datum();
                setTimestampStatus(t, 'highlight', false, this.parentNode);
            })
            .on('click', function() {
                var t = d3.select(this.parentNode).datum();
                toggleTimestampStatus(t, 'focus', this.parentNode);
            });


    }

    function registerEvents() {

        self.eventDispatcher.on(self.constants.event.opensnap, function() {
            cleanSnapShots();
            updateHorizontalPosition();
            updateLinksHPosition();
            updateTimestampsHPosition();
            setTimeout(renderSnapshots, self.config.dftTransitionDuration);
//            renderSnapshots();
        });

        self.eventDispatcher.on(self.constants.event.refocus, function() {
            clean();
            refocus();
            layout();
            render();
            registerCallbacks();
        });

    }

    function display() {
        self.eventDispatcher.fire(self.constants.event.refocus);
    }

    init();

    self.display = display;

}





var SmallMultiples = function(graph, canvas, params) {

    var self = this;

    self.graph = graph;
    self.config = params;
    self.canvas = canvas;

    self.eventDispatcher = new EventDispatcher();

    self.constants = {
        visual: {
            ncolumns : Math.floor((self.config.dftWidth - self.config.dftLeftPadding) / (self.config.dftBlockWidth + self.config.dftHGap))
        },
        event: {
            refocus: 'refocus',
            init: 'init'
        }
    }

    self.state = {
        visual: {
            visible_nodes: []
        },
        cache: {
            subgraph: undefined,
            node_attributes: {}
            // in the form of {nid: {attr0: val0, attr1: val1}},
            // store computed node layout positions, and node degree that varies with time, and nodes' adjacents,

        }
    };

    function init() {
        registerEvents();
    }

    function renderGrids() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            consts = self.constants,
            ts = graph.timestamps();

        canvas.append('g')
            .attr('transform', geom.transform.begin().translate(config.dftLeftPadding, config.dftTopPadding).end())
            .selectAll('.step')
            .data(ts)
            .enter()
            .append('g')
            .attr('class', 'step')
            .attr('transform', function(t, i) {
                var row = Math.floor( i / consts.visual.ncolumns),
                    col = i % consts.visual.ncolumns,
                    x = col * (config.dftBlockWidth + config.dftHGap),
                    y = row * (config.dftBlockHeight + config.dftVGap);

                return geom.transform.begin().translate(x, y).end();
            });

        canvas.selectAll('.step')
            .append('rect')
            .attr('class', 'step_substrate')
            .attr('x', 0).attr('y', 0)
            .attr('width', config.dftBlockWidth).attr('height', config.dftBlockHeight);

    }

    function renderTimestamps() {

        var canvas = self.canvas;

        canvas.selectAll('.step')
            .append('text')
            .attr('class', 'timestamp_label')
            .attr('x', 0).attr('y', -4)
            .text(function(t) {
                return t.toString();
            });

    }

    function renderNLDiagrams() {

        var graph = self.graph,
            config = self.config,
            canvas = self.canvas,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        canvas.selectAll('.step')
            .append('g')
            .attr('class', 'nodelink')
            .each(function(t) {

                var g = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), t);

                d3.select(this)
                    .append('g')
                    .attr('class', 'links')
                    .selectAll('.link')
                    .data(g.getLinks())
                    .enter()
                    .append('path')
                    .attr('class', 'link')
                    .attr('d', function(eid) {
                        var sid = g.link(eid).source,
                            tid = g.link(eid).target;
                        return geom.path.begin()
                            .move_to(cache.node_attributes[sid].x, cache.node_attributes[sid].y)
                            .line_to(cache.node_attributes[tid].x, cache.node_attributes[tid].y).end();
                    })
                    .attr('stroke-width', function(eid) {
                        if (graph.getLinkAttr('t', eid) == t) {
                            var d = graph.getLinkAttr('dist', eid);
                            return config.dftLinkWidthScalingFunc(d);
                        }
                        else {
                            return config.dftMinLinkWidth;
                        }
                    })
                    .attr('stroke', function(eid) {
                        if (graph.getLinkAttr('t', eid) == t) {
                            var d = graph.getLinkAttr('dist', eid);
                            return config.dftLinkColorFunc(d);
                        }
                        else
                            return config.dftLinkStrokeColor;
                    }
                        );

                d3.select(this)
                    .append('g')
                    .attr('class', 'nodes')
                    .selectAll('.node')
                    .data(g.getNodes().filter(function(nid){return g.degree(nid) > 0;}))
                    .enter()
                    .append('g')
                    .attr('class', 'node')
                    .attr('transform', function(nid) {
                        return geom.transform.begin().translate(
                            cache.node_attributes[nid].x,
                            cache.node_attributes[nid].y
                        ).end();
                    })
                    .append('circle')
                    .attr('class', 'node_circle_drawn')
                    .attr('cx', 0).attr('cy', 0)
                    .attr('r', 6.0);

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .append('circle')
                    .attr('class', 'node_circle')
                    .attr('cx', 0).attr('cy', 0)
                    .attr('r', 16.0);

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .append('text')
                    .attr('x', 8).attr('y', 0)
                    .attr('class', 'node_label')
                    .text(function(nid) {
                        return graph.node(nid).name;
                    });

                d3.select(this).select('.nodes')
                    .selectAll('.node')
                    .selectAll('.node_circle')
                    .on('mouseover', function(nid) {
                        if (g.degree(nid) == 0) { return; }
                        d3.select(this.parentNode)
                            .classed('highlight_source', true);
                        setNodeStatus(nid, 'highlight', true);

                    })
                    .on('mouseout', function(nid) {
                        d3.select(this.parentNode)
                            .classed('highlight_source', false);
                        setNodeStatus(nid, 'highlight', false);
                    })
                    .on('click', function(nid) {
                        if (d3.select(this.parentNode).classed('focus') || d3.select(this.parentNode).classed('focus_source')) {
                            d3.select(this.parentNode).classed('focus_source', false);
                            d3.select(this.parentNode).classed('focus', false);
                            setNodeStatus(nid, 'focus', false);
                            setNodeStatus(nid, 'focus_source', false);
                        }
                        else {
                            d3.select(this.parentNode).classed('focus_source', true);
                            d3.select(this.parentNode).classed('focus', true);
                            setNodeStatus(nid, 'focus', true);
                        }
                    });
            })
    }

    function updateDataCache() {

        var graph = self.graph,
            nids = self.state.visual.visible_nodes,
            cache = self.state.cache;

        for (var i = 0, ii = nids.length; i < ii; i ++) {
            cache.node_attributes[nids[i]] = {};
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            cache.node_attributes[nids[i]]['t_degree'] = graph.degreeByTime(nids[i]);
        }

        cache.subgraph = graph.getTemporalAggregatedSubGraph(nids, graph.t0(), graph.t1());

    }

    //use d3 to layout nodes
    function updateLayout() {

        var config = self.config,
            subgraph = self.state.cache.subgraph,
            cache = self.state.cache;

        var rawlinks = subgraph.getLinks(),
            links = [];

        var rawnids = self.state.visual.visible_nodes,
            nodes = [];

        for (var i = 0, ii = rawnids.length; i < ii; i ++) {
            nodes.push({id: rawnids[i]});
        }

        for (var i = 0, ii = rawlinks.length; i < ii; i ++ ) {
            links.push({
                source: subgraph.nodeIdx[subgraph.link(rawlinks[i]).source],
                target: subgraph.nodeIdx[subgraph.link(rawlinks[i]).target]});
        }

        var force = d3.layout.force()
            .gravity(.05)
            .distance(50)
            .charge(-50)
            .size([config.dftBlockWidth, config.dftBlockHeight]);

        force.nodes(nodes)
            .links(links);

        force.start();
        for (var i = 0; i < config.dftMaxIteration; ++i) force.tick();
        force.stop();

        for (var i = 0,  ii = nodes.length; i < ii; i ++) {
            var n = nodes[i];
            cache.node_attributes[n.id].x = n.x;
            cache.node_attributes[n.id].y = n.y;
        }

        geom.scaling(config.dftBlockWidth, config.dftBlockHeight, rawnids,
            function (nid) {
                return cache.node_attributes[nid];
            },
            function (pos, nid) {
                cache.node_attributes[nid].x = pos.x;
                cache.node_attributes[nid].y = pos.y;
            });

    }

    function setNodeStatus(nid, status, flag) {

        var canvas = self.canvas;

        canvas.selectAll('.step')
            .each(function() {
                d3.select(this).selectAll('.nodes')
                    .selectAll('.node')
                    .each(function (_nid) {
                        if (nid == _nid) {
                            d3.select(this)
                                .classed(status, flag);
                        }
                    });
            });
    }

    function clean() {
        canvas.selectAll('g').remove();
    }

    function cleanNLDiagrams() {
        canvas.selectAll('.nodelink').remove();
    }

    function registerEvents() {

        self.eventDispatcher.on(self.constants.event.refocus, function() {
            cleanNLDiagrams();
            updateDataCache();
            updateLayout();
            renderNLDiagrams();
        });

        self.eventDispatcher.on(self.constants.event.init, function() {
            clean();
            renderGrids();
            renderTimestamps();
        });
    }

    function init_display() {
        self.eventDispatcher.fire(self.constants.event.init);
    }

    init();

    self.init_display = init_display;

}


/**
 * Created by panpan on 6/7/14.
 */

var app = {};

var drawHierarchy = function() {

    d3.json('data/graph/dblp_vis_coauthor_community_layout.json', function (data) {
        var g = new Graph();
        for (var i = 0, ii = data.links.length; i < ii; i ++) {
            g.addLink(i, data.links[i][0], data.links[i][1], data.links[i][2]);
        }
        app.canvas_hierarchy  = d3.select('#hierarchy').append('svg')
            .style('width', '100%')
            .style('height', '98%');
        app.hierarchy = new Dendrogram(data.tree, g, app.graph, app.canvas_hierarchy, config.visual.hierarchy);
        app.hierarchy.render();
    });

}

$(function(){
    drawHierarchy();
});







