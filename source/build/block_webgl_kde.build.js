/**
 * Created by panpan on 4/16/14.
 */
functools = function () {
    var f = {
        version: '0.0',
        identity: function(x) {return x;},
        //addMethod (for javascript function overload)  - By John Resig (MIT Licensed)
        addMethod: function (object, name, fn) {
            var old = object[ name ];
            object[ name ] = function () {
                if (fn.length == arguments.length)
                    return fn.apply(this, arguments);
                else if (typeof old == 'function')
                    return old.apply(this, arguments);
            };
        }
    };
    return f;
}();

algo = function () {

    var al ={
        version: "0.0"
    };

    //list generator from another list
    al.listgen = function(array, accessor, context) {

        context = context || null;

        var rs = [];
        for (var i= 0, len=array.length; i < len; i+=1) {
            rs.push(accessor.call(context, array[i]));
        }
        return rs;
    };

    //accessor
    al.permutation = function(array, accessor, context, ascending) {
        var order = al.order(array, accessor, context, ascending);
        var permute = Array(array.length);
        order.forEach(function(d, i) {permute[d] = i});
        return permute;
    };

    al.order = function(array, accessor, context, ascending) {

        accessor = accessor || functools.identity;
        context = context || null;
//        ascending = ascending || true;

        var order = [];
        for (var i= 0, ii = array.length; i < ii; i++) {
            order.push(i);
        }

        order.sort(function(i, j) {
            return accessor.call(context, array[i]) - accessor.call(context, array[j]);
        });
        if (ascending == false)
            order.reverse();
        return order;
    };

    al.sort = function(array, accessor, context, ascending) {

        accessor = accessor || functools.identity;
        context = context || null;
        ascending = ascending || true;

        array.sort(function(a, b) {
            return accessor.call(context, a) - accessor.call(context, b);
        });
        if (ascending == false)
            array.reverse();
    };

    al.find_interval = function(val, array, accessor, context) {

        var n = array.length;

        if (n==0) { return -1;}
        if (val > accessor.call(context, array[n-1])) { return n; }
        if (val < accessor.call(context, array[0])) { return -1; }

        var p = 0,
            q = n-1;

        while(p <= q) {
            var m = Math.floor((p + q) / 2);
            var x = accessor.call(context, array[m]);
            if (val > x)
                p = m+1;
            else if (val < x)
                q = m-1;
            else
                return m;
        }
        return q;
    };

    al.interpolate_step = function(val, array, accessor, mapper, context) {

        context = context || null;

        var idx = algo.find_interval(val, array, accessor, context);

        if (idx == -1 || idx == array.length) {
            return null;
        }
        else {
            return mapper.call(context, array[idx]);
        }
    }

    al.find = function(val, array, accessor, context) {
        var idx = al.find_interval(val, array, accessor, context);
        if (idx == -1 || idx == array.length)
            return -1;
        else if (accessor.call(context, array[idx]) == val)
            return idx;
        else
            return -1;
    };

    al.find_element = function(val, array, accessor, context) {
        var idx = al.find(val, array, accessor, context);
        if (idx == -1)
            return null;
        else
            return array[idx];
    };

    al.argmin = function(array, accessor, context) {
        if (array.length == 0)
            return undefined;

        accessor = accessor || functools.identity;
        context = context || null;

        var min = accessor.call(context, array[0]),
            arg = array[0];

        for (var i=1, len=array.length; i < len; i++) {
            var val = accessor.call(context, array[i]);
            if (val < min) {
                min = val;
                arg = array[i];
            }
        }
        return arg;
    };

    al.argmax = function(array, accessor, context) {

         if (array.length == 0)
            return undefined;

        accessor = accessor || functools.identity;
        context = context || null;

        var max = accessor.call(context, array[0]),
            arg = array[0];

        for (var i= 1, len=array.length; i < len; i++) {
            var val = accessor.call(context, array[i]);
            if (val > max) {
                max = val;
                arg = array[i];
            }
        }
        return arg;
    };

    al.min = function(array, accessor, context) {

        context = context || null;
        accessor = accessor || functools.identity;

        var arg = al.argmin(array, accessor, context);
        if (arg == undefined)
            return arg;
        else
            return accessor.call(context, arg);
    };

    al.max = function(array, accessor, context) {

        context = context || null;
        accessor = accessor || functools.identity;

        var arg = al.argmax(array, accessor, context);
        if (arg == undefined)
            return arg;
        else
            return accessor.call(context, arg);
    };

    return al;

}();


/**
 * Created by user on 4/16/14.
 */
color = function() {
    var c = {
        version: '0.0',
        //CIE RGB to CIE xyY conversion
        rgb_to_xyY: function(r, g, b) {
            if (!(r<1.0 && g<1.0 && b<1.0)) {
                r = r / 255;
                g = g / 255;
                b = b / 255;
            }
            var s = 1/0.17697,
                X = s * (0.49 * r + 0.31*g + 0.20 * b),
                Y = s * (0.17697 * r + 0.81240*g + 0.01063* b),
                Z = s * (0.00 * r + 0.01 * g + 0.99 * b);
            var xx = X / (X + Y + Z),
                yy = Y / (X + Y + Z);
            return {x: xx, y: yy , Y: Y};
        },
        xyY_to_rgb: function(x, y, YY) {
            var X = YY / y * x,
                Z = YY / y * (1 - x - y);
            var r = 0.41847 * X + (-0.15866) * YY + (-0.082835) * Z,
                g = (-0.091169) * X + 0.25243 * YY + 0.015708 * Z,
                b = 0.00092090 * X + (-0.0025498) * YY + 0.17860 * Z;
            return {r: r * 255, g: g * 255, b: b * 255};
        }
    }
    return c;
}();

EventDispatcher = function() {
    this.listeners = {};
};

var eproto = EventDispatcher.prototype;

eproto.on = function(event, f) {
    var ls = this.listeners;
    if (!ls.hasOwnProperty(event))
        ls[event] = [];
    ls[event].push(f);
    return this;
}

eproto.off = function(event, f) {
    //TODO: -low turn off listeners
    return this;
}

//pass object as args
eproto.fire = function(event, args) {
    var ls = this.listeners;
    if (!ls.hasOwnProperty(event))
        return;
    var fs = ls[event];
    for (var i= 0, ii=fs.length;i<ii;i+=1) {
        fs[i].call(this, args);
    }
}


/**
 * Created by user on 4/16/14.
 */
geom = function () {
    var g = {version: "0.0"};

    //please be careful of the applying orders
    g.transform = {
        value: '',
        begin: function() {
            this.value = '';
            return this;
        },
        end: function() {
            return this.value;
        },
        translate: function(dx, dy) {
            this.value += 'translate(' + dx + ',' + dy + ')';
            return this;
        },
        rotate: function(theta, x0, y0) {
            this.value += 'rotate(' + theta + ',' + x0 + ',' + y0 + ')';
            return this;
        },
        scale: function(fx, fy) {
            this.value += 'scale(' + fx + ',' + fy + ')';
            return this;
        }
    };

    /*
     get a path string by chaining functions
     example:
     g.path.begin() [.move_to(args), ...] .end()
    */
    g.path = {
        value:'',
        x:0,
        y:0,
        s: 0.5, //for curve easing
        begin: function(){
            this.value = '';
            return this;
        },
        move_to: function(x, y) {
            this.value += ' M ' + x + ' ' + y;
            this.x = x;
            this.y = y;
            return this;
        },
        line_to: function(x, y) {
            this.value += ' L ' + x + ' ' + y;
            this.x = x;
            this.y = y;
            return this;
        },
        eased_line_to: function(x, y) {
            var c0x = this.x,
                c0y = this.y,
                c1x = x,
                c1y = y;
            if ((x-this.x) * (y-this.y) > 0) {
                c0y = this.y * (1 - this.s) + y * this.s;
                c1x = this.x * this.s + x * (1 - this.s);
            }
            else {
                c0x = this.x * (1 - this.s) + x * this.s;
                c1y = this.y * this.s + y * (1 - this.s);
            }
            this.bezier_to(c0x, c0y, c1x, c1y, x, y);
            return this;
        },
        h_eased_line_to: function(x, y) {
            this.bezier_to(this.x * (1-this.s) + x * this.s, this.y, this.x * this.s + x * (1-this.s) , y, x, y);
            return this;
        },
        horizontal_to: function (x) {
            this.x = x;
            return this.line_to(x, this.y);
        },
        vertical_to: function(y) {
            this.y = y;
            return this.line_to(this.x, y);
        },
        horizontal_to_relative: function(x) {
            this.value += ' h ' + x;
            this.x = this.x + x;
            return this;
        },
        vertical_to_relative: function(y) {
            this.value += ' v ' + y;
            this.y = this.y + y;
            return this;
        },
        bezier_to: function(cx0, cy0, cx1, cy1, x1, y1) {
            this.x = x1;
            this.y = y1;
            this.value += ' C ' + cx0  + ',' + cy0 + ' ' + cx1 + ', ' + cy1 + ' ' + x1 + ', ' + y1;
            return this;
        },
        close_path: function() {
            this.value += ' Z ';
            return this;
        },
        end: function() {
            return this.value;
        }
    }

    g.scaling = function(width, height, objects, getter, setter) {

        var x_min = algo.min(objects, function(n) {return getter.call(null, n).x;}),
            x_max = algo.max(objects, function(n) {return getter.call(null, n).x;}),
            y_min = algo.min(objects, function(n) {return getter.call(null, n).y;}),
            y_max = algo.max(objects, function(n) {return getter.call(null, n).y;});

        var s = Math.min(width / (x_max - x_min), height / (y_max - y_min)),
            dx = (width - s * (x_max - x_min)) / 2,
            dy = (height - s * (y_max - y_min)) / 2;

        for (var i = 0, ii = objects.length; i < ii; i++) {
            setter.call(null,
                {x: ((getter.call(null, objects[i]).x - x_min) * s + dx),
                    y: ((getter.call(null, objects[i]).y - y_min) * s + dy)},
                objects[i]);
        }
    }

    return g;
}();

/*code snippets from threejs webgl_gpgpu_birds example
* need to include three.js
* */

var glUtil = (function() {

    var glu = {

        generateTexture: function(data, format, type, width, height) {

            var texture = new THREE.DataTexture(data, width, height, format, type);
            texture.minFilter = THREE.LinearFilter;
            texture.magFilter = THREE.LinearFilter;
            texture.needsUpdate = true;
            texture.flipY = false;

            return texture;

        },

        getRenderTarget: function(width, height, format, type) {

            var renderTarget = new THREE.WebGLRenderTarget(width, height, {
                wrapS: THREE.ClampToEdgeWrapping,
                wrapT: THREE.ClampToEdgeWrapping,
                minFilter: THREE.LinearFilter,
                magFilter: THREE.LinearFilter,
                format: format,
                type: type,
                stencilBuffer: false
            });

            return renderTarget;
        }

    }

    return glu;
})();


var glColor = (function(){

    var glc = {

        getYToGTexture: function(nColors) {
            var color1 = {h: (60 / 360), s: 1.0, l: 0.9},
                color0 = {h: (228 / 360), s: 0.3, l: 0.2};

            var nColors = nColors || 128;

            var colorData = new Float32Array(nColors * 4),
                colorTexture;

            for (var i= 0, ii=nColors; i < ii; i++) {
                var frac = i * 1.0 / ii;
                var c = new THREE.Color();
                c.setHSL(color0.h * (1 - frac) + color1.h * frac, color0.s * (1 - frac) + color1.s * frac, color0.l * (1 - frac) + color1.l * frac);

                colorData[i * 4] = c.r;
                colorData[i * 4 + 1] = c.g;
                colorData[i * 4 + 2] = c.b;
                colorData[i * 4 + 3] = 1.0;
            }

            colorTexture = glUtil.generateTexture(colorData, THREE.RGBAFormat, THREE.FloatType, nColors, 1);

            return colorTexture;
        }
    };

    return glc;
})();




/**
 * Created by user on 4/16/14.
 */
matrix = function () {
    var m = {
        version: '0.0',
        consts: {
            ZERO: 1e-9
        },
        //initialize a zero matrix with m rows and n columns
        zeros : function (m, n) {
            var mat = [];
            for (var i= 0; i < m; i++) {
                var row = [];
                mat.push(row);
                for (var j = 0; j < n; j++) {
                    row.push(0);
                }
            }
            return mat;
        },
        identity: function(m) {
            var mat = this.zeros(m, m);
            for (var i = 0; i < m; i++) {
                mat[i][i] = 1;
            }
            return mat;
        },
        trace: function(mat) {
            var tr = [];
            for (var i = 0, ii = mat.length; i < ii; i++) {
                tr.push(mat[i][i]);
            }
            return tr;
        },
        col: function(j, mat) {
            var entries = [];
            for (var i = 0, ii = mat.length; i < ii; i ++) {
                entries.push(mat[i][j]);
            }
            return entries;
        },

        //return eigenvectors and eigenvalues
        //depend on science.lin.decompose routine in the package science.js
        //https://github.com/jasondavies/science.js/
        eigen: function(mat) {
            var decomposefunc = science.lin.decompose();
            var rs = decomposefunc.call(null, mat);
            var es = this.trace(rs.D)
                o = algo.order(es);
            var ev = [];
            for (var i = 0, ii = o.length; i < ii; i ++) {
                ev.push(this.col(o[i], rs.V));
            }
            algo.sort(es);
            //IDX is the first nonzero eigenvalue
            var i = 0;
            for (ii = es.length; i < ii; i ++) {
                if (Math.abs(es[i]) > this.consts.ZERO)
                    break;
            }
            return {E : es, V: ev, IDX: i};
        },
//      NOTE: we have to find rectangular area
        find_maximum_rectangle: function(mat) {
            //find rectangle with maximum area in the matrix
            var nrows = m.get_num_rows(mat),
                ncols = m.get_num_cols(mat),
                s = m.zeros(nrows, ncols),
                i0 = 0,
                j0 = 0,
                height = 0,
                width = 0;

            if (nrows == 0 || ncols == 0) return null;

            //histogram
            for (var i=0; i < nrows; i++) {
                var cnt = 0;
                for (var j=ncols-1; j >= 0; j--) {
                    if (mat[i][j] > 0)
                        cnt ++;
                    else
                        cnt = 0;
                    s[i][j] = cnt;
                }
            }

            var stack = [],
                area = 0;

            //maximum rectangle area in a histogram
            for (var j=0; j < ncols; j++) {
                var i = 0;
                while(i < nrows) {
                    if(stack.length == 0 || s[stack[stack.length-1]][j] <= s[i][j])
                        stack.push(i++);
                    else {
                        var tp = stack[stack.length-1];
                        stack.pop();
                        //stack is empty, all previous values larger than s[i][j]
                        //tp is the most previous one
                        if (stack.length == 0) {
                            if (i * s[tp][j] > area) {
                                area = i * s[tp][j];
                                i0 = tp;
                                j0 = j;
                                width = s[tp][j];
                                height = i;
                            }
                        }
                        else {
                            if ((i - stack[stack.length-1] -1) * s[tp][j] > area) {
                                area = (i - stack[stack.length-1] -1) * s[tp][j];
                                i0 = stack[stack.length-1] + 1;
                                j0 = j;
                                width = s[tp][j];
                                height = i - stack[stack.length-1] -1;
                            }
                        }
                    }
                }

                while (stack.length > 0) {
                   var tp = stack[stack.length-1];
                    stack.pop();
                    if (stack.length == 0) {
                        if (i * s[tp][j] > area) {
                            area = i * s[tp][j];
                            i0 = tp;
                            j0 = j;
                            width = s[tp][j];
                            height = i;
                        }
                    }
                    else {
                        if ((i - stack[stack.length-1] -1) * s[tp][j] > area) {
                            area = (i - stack[stack.length-1] -1) * s[tp][j];
                            i0 = stack[stack.length-1] + 1;
                            j0 = j;
                            width = s[tp][j];
                            height = i - stack[stack.length-1] -1;
                        }
                    }
                }
            }

            return {i0: i0, j0: j0, height: height, width: width};
        },
        clean_rectangle: function(i0, j0, height, width, mat) {
            for (var i=i0, ii=height + i0; i < ii; i++) {
                for (var j=j0, jj=width + j0; j < jj; j++) {
                    mat[i][j] = 0;
                }
            }
            return mat;
        },
        is_zero: function(mat) {
            var flag = true;
            for (var i= 0, ii= m.get_num_rows(mat); i < ii; i++) {
                for (var j= 0, jj= m.get_num_cols(mat); j < jj; j++) {
                    if (mat[i][j] != 0)
                        flag = false;
                }
            }
            return flag;
        },
        //find the nearest empty entry in the matrix with a spiral order
        find_nearest_empty_entry: function (i, j, mat) {
            if (i<0||i>=m.get_num_rows(mat)||j<0||j>=m.get_num_cols(mat)) {
                var ii = i,
                    jj = j;
                if (i < 0) ii = 0;
                if (i>= m.get_num_rows(mat)) ii = m.get_num_rows(mat) - 1;
                if (j < 0) jj = 0;
                if (j>= m.get_num_cols(mat)) jj = m.get_num_cols(mat) - 1;
                return {i: ii, j: jj};
            }
            var ii = i,
                jj = j,
                k = 0;
            while (mat[ii][jj] != 0) {
                if (ii == i - k && jj == j - k ) {
                    ii -= 1;
                    k += 1;
                }
                else if (ii == i - k && jj < j + k) {
                    jj += 1;
                }
                else if (jj == j + k && ii < i + k) {
                    ii += 1;
                }
                else if (ii == i + k && jj > j - k) {
                    jj -= 1;
                }
                else if (jj == j - k && ii > i - k) {
                    ii -= 1;
                }

                if (ii<0||ii>=m.get_num_rows(mat)||jj<0||jj>=m.get_num_cols(mat)) {
                    if (ii < 0) ii = 0;
                    if (ii>= m.get_num_rows(mat)) ii = m.get_num_rows(mat) - 1;
                    if (jj < 0) jj = 0;
                    if (jj>= m.get_num_cols(mat)) jj = m.get_num_cols(mat) - 1;
                    return {i: ii, j: jj};
                }

            }
            return {i: ii, j: jj};
        },
        get_num_rows: function(mat) {
            return mat.length;
        },
        get_num_cols: function(mat) {
            if (mat.length == 0)
                return 0;
            else
                return mat[0].length;
        },
        to_string: function(mat) {
            var str = '';
            var nrows = m.get_num_rows(mat),
                ncols = m.get_num_cols(mat);
        },
        //test spiral
        test_spiral: function() {
            var mat = matrix.zeros(7, 7);
            for (var i = 0, ii = 36; i < ii; i++) {
                var entry = m.find_nearest_empty_entry(3, 3, mat);
                mat[entry.i][entry.j] = 1;
                console.info(entry);
            }
        },
        //test finding maximum rectangle
        //passed simple test, should be ok
        test_max_rectangle: function() {
            var mat = matrix.zeros(8, 8);
            for (var i = 0, ii = 30; i < ii; i++) {
                var entry = m.find_nearest_empty_entry(3, 3, mat);
                mat[entry.i][entry.j] = 1;
            }
            console.info(mat);
            m.find_maximum_rectangle(mat);
        }
    };
    return m;
}();

/*
 created by panpan xpp2007@gmail.com
 */

Graph = function() {

    this.empty();

};

_.extend(Graph.prototype, {

    empty: function() {

        this.nodes = [];
        this.links = [];
        this.adjlinks = [];

        this.nodeIdx = {};
        this.linkIdx = {};

        this.invalidNodeIndices = [];
        this.nextNodeIdx = 0;
        this.invalidEdgeIndices = [];
        this.nextEdgeIdx = 0;

        return this;
    },

    addNode: function(nid, params) {

        //TODO: check if nid is already in

        var n = {
            id: nid + '',
            name: '',
            attrs: {}
        };

        var nidx = -1;

        if (this.invalidNodeIndices.length == 0) {
            this.nodes.push(undefined);
            this.adjlinks.push(undefined);
            nidx = this.nextNodeIdx;
            this.nextNodeIdx ++;
        } else {
            nidx = this.invalidNodeIndices.pop();
        }

        this.nodes[nidx] = n;
        this.adjlinks[nidx] = [];
        this.nodeIdx[nid] = nidx;
        this.updateNodeAttrs(params, nid);
        return this;
    },


    addLink: function(eid, source, target, params) {

        var e = {
            id: eid + '',
            source: source + '',
            target: target + '',
            attrs: {}
        };

        if (this.node(source) == undefined) {
            this.addNode(source);
        }
        if (this.node(target) == undefined) {
            this.addNode(target);
        }

        var eidx = -1;

        if (this.invalidEdgeIndices.length == 0) {
            this.links.push(null);
            eidx = this.nextEdgeIdx;
            this.nextEdgeIdx ++;
        } else {
            eidx = this.invalidEdgeIndices.pop();
        }

        var sidx = this.nodeIdx[source],
            tidx = this.nodeIdx[target];

        this.links[eidx] = e;
        this.linkIdx[eid] = eidx;

        this.adjlinks[sidx].push(eid);
        this.adjlinks[tidx].push(eid);

        this.updateLinkAttrs(params, eid);
        return this;
    },

    removeNode:  function(nid) {
        //remove links first
        for (var i= 0, adjs=this.adjacents(nid), ii = adjs.length; i< ii; i++) {
            this.removeLink(adjs[i]);
        }
        var nidx = this.nodeIdx[nid];
        this.invalidNodeIndices.push(nidx);
        this.nodes[nidx] = undefined;
        this.adjlinks[nidx] = [];
        return this;
    },

    removeLink: function(arg0, arg1) {
        var e = undefined,
            eidx = -1;
        if (arguments.length == 1) {
            e = this.link(arg0);
            eidx = this.linkIdx[arg0];
        }
        else if (arguments.length >= 2) {
            e = this.link(arg0, arg1);
            eidx = this.linkIdx[e.id];
        }
        this.invalidEdgeIndices.push(eidx);
        this.links[eidx] = undefined;
        //remove from adjacency list
        var s = this.adjlinks[this.nodeIdx[e.source]],
            t = this.adjlinks[this.nodeIdx[e.target]];

        s.splice(s.indexOf(e.id), 1);
        t.splice(t.indexOf(t.id), 1);

        return this;
    },

    node: function(nid) {
        return this.nodes[this.nodeIdx[nid]];
    },

    //overloaded function link
    link: function(arg0, arg1) {
        if (arguments.length == 1) //eid
            return this.links[this.linkIdx[arg0]];
        else if (arguments.length == 2) { //nid0, nid1
            var nlinks = this.adjacents(arg0),
                rs = [];
            for (var i= 0, ii = nlinks.length; i < ii; i ++) {
                if (this.link(nlinks[i]).source == arg0 && this.link(nlinks[i]).target == arg1
                    || this.link(nlinks[i]).source == arg1 && this.link(nlinks[i]).target == arg0) {
                    if(rs.indexOf(nlinks[i]) == -1)
                        rs.push(nlinks[i]);
                }
            }
            if (rs.length == 1) {
                return rs[0];
            }
            else if (rs.length == 0) {
                return null;
            }
            else {
                return rs; //for a multi-graph, return all the links between two nodes
            }
        }
        else
            return undefined;
    },

    neighbor: function(nid, eid) {
        if (nid == this.link(eid).source)
            return this.link(eid).target;
        else
            return this.link(eid).source;
    },

    adjacents: function(nid) {
        return this.adjlinks[this.nodeIdx[nid]];
    },

    neighbors: function(nid) {

        var eids = this.adjacents(nid),
            nbrs = [];

        for (var i= 0, ii=eids.length; i < ii; i ++) {
            var nbr = this.neighbor(nid, eids[i]);
            if (nbrs.indexOf(nbr) == -1)
                nbrs.push(nbr);
        }

        return nbrs;
    },

    degree: function(nid) {
        return this.adjacents(nid).length;
    },

    precedessors: function(nid) { },
    successors: function (nid) { },

    indegree: function(nid) { },
    outdegree: function(nid) { },


    getNodes: function() {
        var nids = [],
            invalids = this.invalidNodeIndices.sort();
        for (var i = 0, ii = this.nodes.length, j = 0; i < ii; i ++) {
            if (j < invalids.length && i < invalids[j] || j >= invalids.length) {
                nids.push(this.nodes[i].id);
            }
            else {
                j ++;
            }
        }
        return nids;
    },

    getLinks: function() {
        var eids = [],
            invalids = this.invalidEdgeIndices.sort();
        for (var i = 0, ii = this.links.length, j = 0; i < ii; i ++) {
            if (j < invalids.length && i < invalids[j] || j >= invalids.length) {
                eids.push(this.links[i].id);
            }
            else {
                j ++;
            }
        }
        return eids;
    },

    getNodeAttr: function(attr, nid) {
        return this.node(nid).attrs[attr];
    },

    getNodeAttrs: function(attrs, nid) {
        var rs = {};
        for (var i= 0, ii=attrs.length; i < ii; i ++) {
            rs[attrs[i]] = this.getNodeAttr(attrs[i], nid);
        }
        return rs;
    },

    getLinkAttr: function(attr, eid) {
        return this.link(eid).attrs[attr];
    },

    bfs: function(nid, dlimit) {

        var visited = d3.set([nid]),
            distances = d3.map(),
            queue = [nid];

        distances.set(nid, 0);

        while (queue.length > 0) {
            var n0 = queue.shift(),
                d = distances.get(n0);
            if (d >= dlimit)
                continue;
            var nbrs = this.neighbors(n0);
            for (var i=0, ii=nbrs.length; i<ii; i++) {
                if (!visited.has(nbrs[i])) {
                    queue.push(nbrs[i]);
                    visited.add(nbrs[i]);
                    distances.set(nbrs[i], d + 1);
                }
            }
        }

        return distances;
    },

    updateNodeAttrs: function(params, nid) {

        var n = this.node(nid);
        params = params || {};
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                switch (k) {
                    case 'id': break;
                    case 'name': n.name = params[k]; break;
                    default :n.attrs[k] = params[k]; break;
                }
            }
        }
    },

    updateLinkAttrs: function(params, eid) {

        var e = this.link(eid);
        params = params || {};
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                switch (k) {
                    case 'id': break;
                    case 'source': break;
                    case 'target': break;
                    default :e.attrs[k] = params[k]; break;
                }
            }
        }
    },
    //adjacency matrix based on the given order
    denseAdjacencyMatrix: function(nids) {

        var mat = matrix.zeros(nids.length, nids.length);

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i + 1, jj = nids.length; j < jj; j++) {
                var e = this.link(nids[i], nids[j]);
                if (_.isArray(e)) {
                    mat[j][i] = mat[i][j] = e.length;
                }
                else if (e != null){
                    mat[j][i] = mat[i][j] = 1;
                }
                else {
                }
            }
        }

        return mat;
    },

    laplacianMatrix: function(nids) {

        var mat = this.denseAdjacencyMatrix(nids),
            rowsum = [];

        for (var i = 0, ii = nids.length; i < ii; i++) {
            var sum = 0.0;
            for (var j = 0, jj = nids.length; j < jj; j ++) {
                sum += mat[i][j];
            }
            rowsum.push(sum);
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            mat[i][i] = rowsum[i];
        }

        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = nids.length; j < jj; j ++) {
                mat[i][j] = - mat[i][j];
                mat[j][i] = - mat[j][i];
            }
        }

        return mat;
    },

    //-----------algorithm used in first paper submission---------------------
    bundles: function(nids) {

        var mat = this.denseAdjacencyMatrix(nids),
            bdls = [];

        //matrix
        for (var i = 0, ii = nids.length; i < ii; i++) {
            //fill in diagonal entries
            mat[i][i] = 1;
        }

        //detect rectangle areas and check for rectangles like in issue01.png
        while (!matrix.is_zero(mat)) {
            var rect = matrix.find_maximum_rectangle(mat);
            if (rect == null
                || (rect.height == 0 && rect.width == 0)
                || (rect.height == 1 && rect.width == 1))
                break;

            if (rect.height == 1 && rect.width > 1) {
                if (rect.i0 == rect.j0) {
                    rect.j0 += 1;
                    rect.width -= 1;
                }
                else {
                    if (rect.i0 == rect.j0 + rect.width - 1) {
                        rect.width -= 1;
                    }
                }
            }

            if (rect.width == 1 && rect.height > 1) {
                if (rect.i0 == rect.j0) {
                    rect.i0 += 1;
                    rect.height -= 1;
                }
                else {
                    if (rect.j0 == rect.i0 + rect.height - 1) {
                        rect.height -= 1;
                    }
                }
            }

            if (rect.height > rect.width) {
                bdls.push({
                    i0: rect.j0,
                    i1: rect.j0 + rect.width - 1,
                    j0: rect.i0,
                    j1: rect.i0 + rect.height - 1
                });
            }
            else {
                bdls.push({
                    i0: rect.i0,
                    i1: rect.i0 + rect.height - 1,
                    j0: rect.j0,
                    j1: rect.j0 + rect.width - 1
                });
            }

            matrix.clean_rectangle(rect.i0, rect.j0, rect.height, rect.width, mat);
            matrix.clean_rectangle(rect.j0, rect.i0, rect.width, rect.height, mat);
        }

        //check for size-one rectangles
        for (var i = 0, ii = nids.length; i < ii; i++) {
            for (var j = i, jj = nids.length; j < jj; j++) {
                if (mat[i][j] != 0 && i != j) {
                    bdls.push({
                        i0: i,
                        i1: i,
                        j0: j,
                        j1: j
                    });
                }
            }
        }

        return bdls;
    },


    fuzzy_bundles: function(nids) {

        //TODO detect dense blocks in the adjacency matrix

    },

    info: function() {
        return '#nodes = ' + this.nodes.length + '; ' + '#links = ' + this.links.length;
    }

});
var shaders = {

    vertex_shader_through:
        'void main(void) {' +
        '   gl_Position = vec4(position, 1.0);' +
        '}',

    vertex_shader_points:
        'void main(void) {' +
        '   gl_PointSize = 1.0;' +
        '   gl_Position = vec4(position.xy, 0.0, 1.0);' +
        '}',

    fragment_shader_color_mapping:
        'uniform vec2 resolution;' +
        'uniform vec3 uColor;' +
        'uniform float uNData;' +
        'uniform sampler2D textureDensity;' +
        'uniform sampler2D textureColor;' +
        'void main(void) {' +
        '   float dx = 1.0 / resolution.x;' +
        '   float dy = 1.0 / resolution.y;' +
        '   vec2 uv = gl_FragCoord.xy / resolution.xy;' +
        '   vec2 uvColor = vec2(texture2D(textureDensity, uv).a * 20.0, 0.5);' +
        '   uvColor.s = floor(uvColor.s * 8.0) / 8.0;' +
        '   gl_FragColor = texture2D(textureColor, uvColor);' +
        '}',

    fragment_shader_vconvolution:
        'uniform vec2 resolution;' +
        'uniform sampler2D textureDensity;' +
        'uniform sampler2D textureKernel;' +
        'const float ungrid = uNGrid;' +
        'void main(void) {' +
        '   vec2 uv = gl_FragCoord.xy / resolution.xy;' +
        '   float step = 1.0 / resolution.y;' +
        '   uv.t -= step * (uNGrid - 1.0) / 2.0;' +
        '   vec2 uvKernel = vec2(0.0, 0.5);' +
        '   float stepKernel = 1.0 / uNGrid;' +
        '   uvKernel.s += stepKernel / 2.0;' +
        '   float sum = 0.0;' +
        '   for (float i = 0.0; i < ungrid; i+=1.0) {' +
            '   sum += texture2D(textureDensity, uv).a * texture2D(textureKernel, uvKernel).a;' +
            '   uv.t += step;' +
            '   uvKernel.s += stepKernel;' +
            '}' +
        '   gl_FragColor = vec4(vec3(1.0), sum);' +
        '}',

    fragment_shader_hconvolution:
        'uniform vec2 resolution;' +
        'uniform sampler2D textureDensity;' +
        'uniform sampler2D textureKernel;' +
        'const float ungrid = uNGrid;' +
        'void main(void) {' +
        '   vec2 uv = gl_FragCoord.xy / resolution.xy;' +
        '   float step = 1.0 / resolution.x;' +
        '   uv.s -= step * (uNGrid - 1.0) / 2.0;' +
        '   vec2 uvKernel = vec2(0.0, 0.5);' +
        '   float stepKernel = 1.0 / uNGrid;' +
        '   uvKernel.s += stepKernel / 2.0;' +
        '   float sum = 0.0;' +
        '   for (float i = 0.0; i < ungrid; i+=1.0) {' +
            '   sum += texture2D(textureDensity, uv).a * texture2D(textureKernel, uvKernel).a;' +
            '   uv.s += step;' +
            '   uvKernel.s += stepKernel;' +
            '}' +
        '   gl_FragColor = vec4(vec3(1.0), sum);' +
        '}'


};
var renderDesityMap = function(graph, width, height) {

    var canvas = $('#display canvas')[0];

    var ndata = graph.getNodes().length;

    var colorTexture = glColor.getYToGTexture();

    var stdev = 6.0,
        nGrid = Math.floor(stdev * 4),
        kernelTexture;

    var kernelData = new Float32Array(nGrid * 1);

    var sum = 0.0;
    for (var i = 0, ii= kernelData.length; i < ii; i++ ) {
        kernelData[i] = Math.exp(- Math.pow(i - (ii - 1) / 2 , 2) / Math.pow(stdev, 2) / 2);
        sum += kernelData[i];
    }

    for (var i = 0, ii = kernelData.length; i < ii; i ++) {
        kernelData[i] /= sum;
    }

    kernelTexture = glUtil.generateTexture(kernelData, THREE.AlphaFormat, THREE.FloatType, nGrid, 1);

	var cameraRTT = new THREE.OrthographicCamera(0, width, height,
0, -100, 100);
    cameraRTT.position.z = 100;

    var camera = new THREE.PerspectiveCamera(60,  width/height, 0, 100);
    camera.position.z = 100;

    canvas.style.width = width / window.devicePixelRatio;
    canvas.style.height = height / window.devicePixelRatio;
    canvas.width = width;
    canvas.height = height;
    var renderer = new THREE.WebGLRenderer({alpha: true, canvas: canvas, preserveDrawingBuffer: true});
    renderer.autoClearColor = false;

    var fragmentShaderVConvolution = new THREE.ShaderMaterial({
        uniforms: {
            resolution: { type: "v2", value: new THREE.Vector2(width, height) },
            textureDensity: { type: "t", value: null },
            textureKernel: {type: "t", value: kernelTexture}
        },
		defines: {
			uNGrid: nGrid.toFixed(2)
		},
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_vconvolution,
        transparent: true
    });


    var fragmentShaderHConvolution = new THREE.ShaderMaterial({
        uniforms: {
            resolution: { type: "v2", value: new THREE.Vector2(width, height) },
            textureDensity: { type: "t", value: null },
            textureKernel: {type: "t", value: kernelTexture}
        },
		defines: {
			uNGrid: nGrid.toFixed(2)
		},
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_hconvolution,
        transparent: true
    });


    var fragmentShaderColorMapping = new THREE.ShaderMaterial({
        uniforms: {
            resolution: {type: "v2", value: (new THREE.Vector2(width, height))},
            uNData: {type: "f", value: ndata},
            uColor: {type: "v3", value: (new THREE.Vector3(color.r, color.g, color.b))},
            textureDensity: {type: "t", value: null},
            textureColor: {type: "t", value: colorTexture}
        },
        vertexShader: shaders.vertex_shader_through,
        fragmentShader: shaders.fragment_shader_color_mapping,
        transparent: true
    });

    var rtDensity1 = glUtil.getRenderTarget(width, height, THREE.RGBAFormat, THREE.FloatType),
        rtDensity2 = rtDensity1.clone();

    var stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    $('#display').append(stats.domElement);

    //add data points to geometry
    var points = new THREE.Geometry(),
        pmaterial = new THREE.ParticleSystemMaterial({
            color: 0xFFFFFF,
            size: 1,
            blending: THREE.CustomBlending,
            blendEquation: THREE.AddEquation,
            blendSrc: THREE.OneFactor,
            blendDst: THREE.OneFactor,
            opacity: 0.075,
            transparent: true
        });

    for (var i= 0, nodes = graph.getNodes(), ii = nodes.length; i < ii; i++) {

        var pX = graph.getNodeAttr('x', nodes[i]),
            pY = graph.getNodeAttr('y', nodes[i]),
            pZ = 0,
            point = new THREE.Vector3(pX, pY, pZ);

        points.vertices.push(point);
    }


    var particleSystem = new THREE.ParticleSystem(points, pmaterial);
    var pointsScene = new THREE.Scene();
    pointsScene.add(particleSystem);

    var renderAccumulate = function(output) {
        renderer.render(pointsScene, cameraRTT, output, true);
    };

    var scene = new THREE.Scene();
    var rect = new THREE.PlaneGeometry(width, height),
        mesh = new THREE.Mesh(rect, fragmentShaderColorMapping);
    scene.add(mesh);

    var renderVConvolution = function(input, output) {
        mesh.material = fragmentShaderVConvolution;
        fragmentShaderVConvolution.uniforms.textureDensity.value = input;
        renderer.render(scene, cameraRTT, output, true);
    };

    var renderHConvolution = function(input, output) {
        mesh.material = fragmentShaderHConvolution;
        fragmentShaderHConvolution.uniforms.textureDensity.value = input;
        renderer.render(scene, cameraRTT, output, true);
    };

    //build graph scene
    var lines = new THREE.Geometry(),
        lmaterial = new THREE.LineBasicMaterial(
        {
            color: 0xffffff,
            opacity: 0.02,
            transparent: true,
            linewidth: 1,
            blending: THREE.AdditiveBlending
        }
    );

    for (var i = 0, links = graph.getLinks(), ii = links.length; i < ii; i ++) {
        var e = graph.link(links[i]),
            n0 = graph.node(e.source),
            n1 = graph.node(e.target);

        lines.vertices.push(
            new THREE.Vector3(n0.attrs.x, n0.attrs.y, 0.0));
        lines.vertices.push(
            new THREE.Vector3(n1.attrs.x, n1.attrs.y, 0.0));
    }

    var edges = new THREE.Line(lines, lmaterial, THREE.LinePieces);
    var edgesScene = new THREE.Scene();
    edgesScene.add(edges);

    var renderColorMap = function(input) {
        mesh.material = fragmentShaderColorMapping;
        fragmentShaderColorMapping.uniforms.textureDensity.value = input;
        renderer.render(scene, camera);

    };

    var renderEdges = function() {
        renderer.render(edgesScene, cameraRTT, null, false);
    };

    var render = function () {

        requestAnimationFrame(render);

        renderer.clear(true, true, true);
        renderAccumulate(rtDensity1);
        renderVConvolution(rtDensity1, rtDensity2);
        renderer.clearTarget(rtDensity1, true, true, true);

        renderHConvolution(rtDensity2, rtDensity1);
        renderer.clearTarget(rtDensity2, true, true, true);

        renderColorMap(rtDensity1);
        renderer.clearTarget(rtDensity1, true, true, true);
        stats.update();

        renderEdges();
    };

    render();

}

$(function() {


    //TODO: load graph and show edges in 3D space
    var graph = new Graph();

    //DBLP co-authorship graph
    d3.json('data/dblp_vis_coauthor.json', function(data) {

        for (var i= 0, ii=data.nodes.length; i < ii; i++) {
            graph.addNode(data.nodes[i].id, data.nodes[i]);
        }

        for (var i= 0, ii=data.links.length; i < ii; i++) {
            graph.addLink(i, data.nodes[data.links[i].source].id, data.nodes[data.links[i].target].id, data.links[i]);
        }

        for (var i= 0, nodes = graph.getNodes(), ii = nodes.length; i < ii; i++) {
            var n = graph.node(nodes[i]);
            n.attrs.x = n.attrs.x / 100 + 250 + 250;
            n.attrs.y = n.attrs.y / 100 + 250;
        }

        renderDesityMap(graph, 1024, 512);

    });

});



