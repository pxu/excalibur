if __name__ == '__main__':
    import json
    graph = json.load(open('dblp_vis_coauthor_community.json', 'r'))
    tree = json.load(open('tree_layout.json', 'r'))

    graph['tree'] = tree

    fout = open('dblp_vis_coauthor_community_layout.json', 'w')
    json.dump(graph, fout)
    fout.close()

