/**
 * Created by panpan on 5/24/14.
 */

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        dirs: {
            src: 'js',
            dest: 'build'
        },
        wiredep: {
            app : {
                src: 'index.html'
            }
        },
        concat: {
            options: {
                seperator: ';'
            },
            block_webgl_kde: {
                src: ['js/common/*.js',
                        'js/util/*.js',
                        'js/data/graph.js',
                        'js/block_webgl_kde/app_shader.js',
                        'js/block_webgl_kde/app.js'],
                dest: 'build/block_webgl_kde.build.js'
            },
            application_dynamic_graph: {
                src: ['js/common/algo.js',
                        'js/util/eventdispatcher.js',
                        'js/util/geom.js',
                        'js/util/matrix.js',
                        'js/data/graph.js',
                        'js/data/dynamic_graph.js',
                        'js/vis/nlgraph.js',
                        'js/vis/leafOrder.js',
                        'js/application_dynamic_graph/config.js',
                        'js/application_dynamic_graph/timeline.js',
                        'js/application_dynamic_graph/small_multiples.js',
                        'js/application_dynamic_graph/hierarchy.js',
                        'js/application_dynamic_graph/app.js'],
                dest: 'build/application_dynamic_graph.build.js'
            }

        },

        jshint: {
            files: ['Gruntfile.js'],
            options: { }
        },
        watch :{
            scripts: {
                files: ['js/*.js', 'js/*/*.js', 'js/*/*/*.js'],
                tasks: ['concat']
            }
        }

    });

    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-bower-install");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-wiredep");

    grunt.registerTask('develop', ['concat', 'jshint', 'watch']);
    grunt.registerTask('default', ['concat', 'jshint']);
    grunt.registerTask('wiredep', ['wiredep']);
};