from analysis.cluster.modularity import community_fast_modularity


def get_leaves(root):
    from collections import deque
    q = deque([root])
    leaves = []
    while len(q) > 0:
        node = q.popleft()
        if 'children' in node:
            q.extend(node['children'])
        else:
            leaves.append(node)
    return leaves


if __name__ == "__main__":
    from analysis.data import dblp
    import json
    import networkx as nx
    from analysis.util.logger import *

    g0, g1 = dblp.load_graphs_component0()

    root = community_fast_modularity(g0, tree=True, limit=150)
    leaves = get_leaves(root)

    info('end community detection')

    ga = nx.Graph()

    for i in leaves:
        num_inner_edge = g0.subgraph([n['id'] for n in i['fans']]).number_of_edges()
        i['num_inner_edge'] = num_inner_edge
        for j in leaves:
            number_cross_edges = 0
            ii, jj = i['fans'], j['fans']
            for ci in ii:
                for cj in jj:
                    number_cross_edges += g0.number_of_edges(ci['id'], cj['id'])
            if number_cross_edges != 0:
                ga.add_edge(i['id'], j['id'], weight=number_cross_edges)

    info('end computing aggregated graph')
    json.dump({'tree': root, 'links': ga.edges(data=True)}, open('dblp_vis_coauthor_community.json', 'w'))



