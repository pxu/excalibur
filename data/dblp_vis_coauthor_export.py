if __name__ == '__main__':
    from analysis.data import dblp
    from analysis.data import dynamic_graph
    from networkx.readwrite import json_graph
    import networkx as nx

    g0, g1 = dblp.load_graphs_component0(ifilter={'area': 'vis'})

    dists = dynamic_graph.edge_attr('distance', g0)
    # print(str(len(dists.keys())))
    # print(str(len(g0.edges())))
    for u, v, k, d in g0.edges(data=True, keys=True):
        if k not in dists:
            d['dist'] = -1
        else:
            d['dist'] = dists[k]

    # nx.write_graphml(g0, 'dblp_vis_coauthor.graphml')
    json_graph.dump(g0, open('dblp_vis_coauthor.json', 'w'))





