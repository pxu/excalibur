if __name__ == '__main__':
    import networkx as nx
    import random as rand
    from networkx.readwrite import json_graph

    n, m = 8, 40
    g = nx.MultiGraph()
    for eid in range(m):
        g.add_edge(rand.randint(0, n-1), rand.randint(0, n - 1), key=eid, t=int(eid/4))

    json_graph.dump(g, open('test_' + 'n' + str(n) + '_m' + str(m) + '.json', 'w'))








