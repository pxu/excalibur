if __name__ == '__main__':
    import networkx as nx
    import random as rand
    from networkx.readwrite import json_graph

    n, n2, m, tmax = 6, 10, 6, 9
    g = nx.MultiGraph()
    for i in range(n + n2):
        g.add_node(i)
    eid = 0
    for i in range(n):
        for j in range(n):
            g.add_edge(i, j, key=eid, t=rand.randint(0, tmax-1))
            eid += 1

    for j in range(m):
        g.add_edge(rand.randint(0, n+n2-1), rand.randint(n, n+n2-1), key=eid, t=rand.randint(0, tmax-1))
        eid += 1

    g.add_edge(rand.randint(0, n+n2-1), rand.randint(n, n+n2-1), key=eid, t=rand.randint(0, tmax))


    json_graph.dump(g, open('test_' + 'n' + str(n) + '_n' + str(n2) + '_m' + str(m) + '.json', 'w'))



