from ast import literal_eval


def dump_node_attr(nattrs, G, path, seperator='\t'):
    f = open(path, 'w', encoding='utf-8')
    for n, data in G.nodes(data=True):
        f.write(seperator.join([str(n)] + [str(data[k]) for k in nattrs]) + '\n')
    f.flush()
    f.close()


def dump_edges(G, path, seperator='\t'):
    f = open(path, 'w', encoding='utf-8')
    for u, v, key, data in G.edges(keys=True, data=True):
        f.write(seperator.join([str(w) for w in [key, u, v, data['t']]]) + '\n')
    f.flush()
    f.close()


def load_node_attr(nattrs, G, path, seperator='\t'):
    f = open(path, 'r', encoding='utf-8')
    for l in f:
        vals = l.rstrip().split(seperator)
        n = int(vals[0])
        if len(vals) < len(nattrs) + 1:
            vals += ['.'] * len(nattrs)
        if n not in G:
            G.add_node(n)
        for i, k in enumerate(nattrs):
            G.node[n][k] = vals[i+1]
    f.close()


def load_edges(G, path, seperator='\t'):
    f = open(path, 'r', encoding='utf-8')
    for r in f:
        key, u, v, t = [int(w) for w in r.split(seperator)]
        G.add_edge(u, v, key=key, t=t)
    f.close()


def dump(nattrs, G, path, seperator='\t', nattr_postfix='.nattrs', elist_postfix='.elist'):
    dump_node_attr(nattrs, G, path+nattr_postfix, seperator)
    dump_edges(G, path+elist_postfix, seperator)


def load(nattrs, G, path, seperator='\t', nattr_postfix='.nattrs', elist_postfix='.elist'):
    load_node_attr(nattrs, G, path+nattr_postfix, seperator)
    load_edges(G, path+elist_postfix, seperator)



