from collections import deque


def bfs(G, root, k):

    visited = set([root])
    queue = deque([root])

    while len(visited) < k and len(queue) > 0:
        n0 = queue.popleft()
        for n1 in G.neighbors(n0):
            if n1 not in visited and len(visited) < k:
                queue.append(n1)
                visited.add(n1)

    return list(visited)
