import networkx as nx
from analysis.util.logger import *
from analysis.data.config import *
from analysis.io.graph import *


@infodecor
def conferences(area):
    confs = []
    with open(dblp_processed + area + '.confs') as f:
        for l in f:
            confs.append(l.rstrip())
    return confs


@infodecor
def process(ifilter={'t0':1990, 'area': 'vis'}):

    confs = set(conferences(ifilter['area']))
    authors_idx = dict()
    papers_idx = dict()

    paper_authors = []
    paper_citations = []
    paper_timestamp = []

    with open(dblp_raw, 'r', encoding='utf-8') as f:
        aid = 0
        l = f.readline()
        while len(l) > 0:
            if l.startswith('#@'):
                strs = l[2:].rstrip().split(',')
                while not l.startswith('#conf'):
                    l = f.readline()
                if l.strip()[5:] in confs:
                    for name in strs:
                        if authors_idx.get(name) is None:
                            authors_idx[name] = aid
                            aid += 1
            l = f.readline()

    with open(dblp_raw, 'r', encoding='utf-8') as f:
        l = f.readline()
        while len(l) > 0:
            if l.startswith('#*'):
                author_strs = f.readline()[2:].rstrip().split(',')
                year_str = f.readline()[5:].rstrip()
                year = int(year_str) if len(year_str) != 0 else 0
                conf = f.readline()[5:].rstrip()
                if conf in confs:
                    paper_authors.append([authors_idx[w] for w in author_strs])
                    f.readline()
                    pid = int(f.readline()[6:])
                    citations = []
                    while not l.startswith('#%'):
                        l = f.readline()
                    while l.startswith('#%'):
                        citations.append(int(l[2:]))
                        l = f.readline()
                    paper_citations.append(citations)
                    paper_timestamp.append(year)
                    papers_idx[pid] = len(paper_citations) - 1
            l = f.readline()

    n, m = len(authors_idx.keys()), len(paper_authors)

    g0, g1 = nx.MultiGraph(), nx.MultiDiGraph()
    for name in authors_idx.keys():
        g0.add_node(authors_idx[name], name=name)
    g1.add_nodes_from(g0.nodes(data=True))

    eid = 0
    for i in range(m):
        if paper_timestamp[i] < ifilter['t0']:
            continue
        author_list = paper_authors[i]
        for j0 in author_list:
            for j1 in author_list:
                if j1 > j0:
                    g0.add_edge(j0, j1, eid, t=paper_timestamp[i])
                    eid += 1

    eid = 0
    for i in range(m):
        if paper_timestamp[i] < ifilter['t0']:
            continue
        citation_list = paper_citations[i]
        author_list = []
        for ii in citation_list:
            if ii in papers_idx.keys():
                author_list += paper_authors[papers_idx[ii]]
        for j1 in author_list:
            for j0 in paper_authors[i]:
                g1.add_edge(j1, j0, eid, t=paper_timestamp[i])
                eid += 1

    dump(['name'], g0, dblp_processed + '.'.join([ifilter['area'], 'coauthor']))
    dump(['name'], g1, dblp_processed + '.'.join([ifilter['area'], 'citation']))


def load_graphs(ifilter={'area': 'vis'}):
    g0, g1 = nx.MultiGraph(), nx.MultiDiGraph()
    load(['name'], g0, dblp_processed + '.'.join([ifilter['area'], 'coauthor']))
    load(['name'], g1, dblp_processed + '.'.join([ifilter['area'], 'citation']))
    return g0, g1


def load_graphs_component0(ifilter={'area': 'vis'}):
    g0, g1 = load_graphs(ifilter)
    ns = nx.connected_components(g0)[0]
    g0 = g0.subgraph(ns)
    g1 = g1.subgraph(ns)
    return g0, g1

if __name__ == '__main__':
    process()



