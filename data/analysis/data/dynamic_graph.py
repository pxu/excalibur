import networkx as nx
from analysis.util.logger import *


def aggregate(G):
    assert isinstance(G, nx.MultiGraph)
    g = nx.Graph()
    g.add_edges_from(G.edges())
    return g


def t_range(G):
    assert isinstance(G, nx.MultiGraph)
    edges = G.edges(keys=True, data=True)
    assert 't' in edges[0][3]
    t0 = min((e[3]['t'] for e in edges))
    t1 = min((e[3]['t'] for e in edges))
    return t0, t1


@infodecor
def edge_attr(attr, G):
    assert isinstance(G, nx.MultiGraph)
    if attr == 'distance':
        eids = set([id for u, v, id, d in G.edges(keys=True, data=True)])
        rs = dict()
        g = nx.Graph()
        t0, t1 = t_range(G)
        buffer_edges = []
        for u, v, id, d in sorted(G.edges(keys=True, data=True), key=lambda e: e[3]['t']):
            if d['t'] != t0:
                for _u, _v, _id in buffer_edges:
                    if id in eids:
                        if _u in g and _v in g and nx.has_path(g, _u, _v):
                            rs[_id] = nx.shortest_path_length(g, _u, _v)
                        else:
                            rs[_id] = -1
                for _u, _v, _id in buffer_edges:
                    g.add_edge(_u, _v)
                buffer_edges.clear()
                t0 = d['t']
            buffer_edges.append((u, v, id))
        return rs
    if attr == 'betweenness':
        return None
    return None

@infodecor
def node_time_variant_attr(nodes, attr, G):
    assert isinstance(G, nx.MultiGraph)
    t0, t1 = t_range(G)

    if attr == 'degree':
        rs = dict([(n, []) for n in nodes])
        g = nx.Graph()
        for u, v, id, d in sorted(G.edges(keys=True, data=True), key=lambda e: e[3]['t']):
            if d['t'] != t0:
                for n in nodes:
                    if n in G and (len(rs[n]) == 0 or rs[n][-1][1] < g.degree(n)):
                        rs[n].append((t, len(G[n])))
                t = d['t']
            g.add_edge(u, v)
        for n in nodes:
            if len(rs[n]) > 0 and rs[n][-1][0] != t and n in g:
                rs[n].append((t, len(g[n])))
        return rs
    if attr == 'betweenness-centrality':
        return None
    return None
