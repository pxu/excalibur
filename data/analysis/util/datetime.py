from datetime import datetime, timedelta


def range(t0, t1, step):
    assert isinstance(t0, datetime) and isinstance(t1, datetime) and isinstance(step, timedelta)
    t = t0
    while t < t1:
        yield t
        t += step


def floor_date(t):
    assert isinstance(t, datetime)
    return t - timedelta(hours=t.hour, minutes=t.minute,
                         seconds=t.second, microseconds=t.microsecond)


def ceiling_date(t):
    assert isinstance(t, datetime)
    return floor_date(t) + timedelta(days=1)


def quantize(t, t0, step):
    assert isinstance(t, datetime) and isinstance(t0, datetime) and isinstance(step, timedelta)
    return int((t.timestamp() - t0.timestamp()) / step.total_seconds())