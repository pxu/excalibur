import igraph
import networkx as nx
from analysis.util.logger import *


# use fast modularity maximization routine in igraph
# for community detection

def _get_fans(nodes, ridx):

    from collections import deque
    q = deque([ridx])
    leaves = []

    while len(q) > 0:
        nid = q.popleft()
        if 'children' in nodes[nid]:
            q.extend(nodes[nid]['children'])
        else:
            leaves.append(nodes[nid])

    return leaves


def community_fast_modularity(G, tree=True, limit=100):

    assert isinstance(G, nx.Graph) or isinstance(G, nx.MultiGraph)

    G = nx.Graph(G)
    g = igraph.Graph()
    ns = G.nodes()

    vidx = dict()
    for i, n in enumerate(ns):
        vidx[n] = i

    g.add_vertices(G.number_of_nodes())
    for u, v in G.edges():
        g.add_edge(vidx[u], vidx[v])

    dend = g.community_fastgreedy()

    nodes = []
    for i, n in enumerate(ns):
        nodes.append({'id': n})

    idx = len(ns)
    for u, v in dend.merges:
        nodes.append({'id': idx, 'children': [u, v]})
        idx += 1

    if not tree:
        return nodes

    #construct tree
    if limit >= len(nodes):
        for i in range(len(nodes)-1, -1, -1):
            if 'children' in nodes[i]:
                for j, cidx in enumerate(nodes[i]['children']):
                    nodes[i]['children'][j] = nodes[cidx]
    else:
        for i in range(len(nodes)-1, len(nodes)-1-limit, -1):
            if 'children' in nodes[i]:
                for j, cidx in enumerate(nodes[i]['children']):
                    nodes[i]['children'][j] = nodes[cidx]
                    if cidx <= len(nodes)-1-limit:
                        nodes[cidx]['fans'] = _get_fans(nodes, cidx)
                        if 'children' in nodes[cidx]:
                            nodes[cidx].pop('children')

    return nodes[-1]


#test
if __name__ == "__main__":
    from analysis.data import dblp
    import sys
    import json
    sys.setrecursionlimit(20000)
    g0, g1 = dblp.load_graphs_component0()
    d = community_fast_modularity(g0)
    json.dump(d, open('test', 'w'))
