import networkx as nx
from random import randrange
import numpy as npy
from numpy.linalg import linalg
from analysis.util.logger import *
from networkx.readwrite import json_graph
from sklearn.manifold import MDS

#classical (metric) mds
@infodecor
def classical_mds(dists_mat, d=2):
    m = npy.matrix(dists_mat)
    dim = m.shape
    m = npy.square(m)  # square entries in the matrix
    m = -0.5 * (m - npy.ones(dim).dot(npy.diagflat(m.sum(0))) / dim[0] - npy.diagflat(m.sum(1)).dot(npy.ones(dim)) / dim[1] + npy.ones(dim) * m.sum() / npy.product(dim))  # centering matrix
    es, evs = linalg.eig(m)  # get matrix evs
    ord = sorted(range(dim[0]), key=lambda i: es[i], reverse=True)
    cols = [ord[i] for i in range(d)]
    # the first part is the coordinates of the pivot nodes
    # the second part is used to compute the coordinates of the non-pivot nodes
    coords = evs[:, cols].dot(npy.diagflat(npy.sqrt(es[cols])))
    L = evs[:, cols].dot(npy.diagflat(npy.power(npy.sqrt(es[cols]), -1)))
    mu = m.sum(0) / dim[0]
    return coords, L, mu


#return nodes mapped to d-dimensional positions
@infodecor
def landmark_mds(G, k=1500, d=2):
    #landmark selection with maxmin
    # assert isinstance(G, nx.Graph)
    n0 = G.nodes()[randrange(G.number_of_nodes())]
    pivots, dists_mat = [n0], [nx.single_source_shortest_path_length(G, n0)]
    while len(pivots) < k:
        n0 = max(G.nodes(), key=lambda n: min([dists[n] for dists in dists_mat]))
        pivots.append(n0)
        dists_mat.append(nx.single_source_shortest_path_length(G, n0))
    #layout landmark nodes
    mat = npy.empty((k, k))
    for i in range(k):
        for j in range(k):
            mat[i, j] = dists_mat[i][pivots[j]]
    # print(str(mat))
    pivot_coords, L, mu = classical_mds(mat, d)
    #weighted barycenter as approximate positions from the landmarks
    coords = dict([(pivots[i], list(npy.real(pivot_coords[i, :]).flat)) for i in range(k)])
    pivots_set = set(pivots)
    for n in G.nodes():
        # if not n in pivots_set:
        coords[n] = list(npy.real((- 0.5 * (npy.matrix([dists[n] for dists in dists_mat]) - mu).dot(L))).flat)
    #PCA, re-orient the data
    return coords


def pivot_mds(G, k=250, d=2):
    #TODO:


    pass

@infodecor
def metric_mds(G):
    dists = nx.shortest_paths.floyd_warshall_numpy(G)
    mds = MDS(n_components=2, metric=True, max_iter=3000, dissimilarity="precomputed", n_jobs=2)
    pos = mds.fit(dists).embedding_
    coords = dict()
    for i, n in enumerate(g.nodes()):
        coords[n] = pos[i]
    return coords



#note: G should be connected
@infodecor
def nonmetric_mds(G):
    info('\tcomputing all pair shortest path')
    dists = nx.shortest_paths.floyd_warshall_numpy(G)
    info('\tfinished.')
    mds = MDS(n_components=2, metric=False, max_iter=1000, dissimilarity="precomputed", n_jobs=2)
    pos = mds.fit(dists).embedding_
    coords = dict()
    for i, n in enumerate(g.nodes()):
        coords[n] = pos[i]
    return coords


if __name__ == '__main__':

    g = nx.read_gml('../../jdk_gelphi_radial.gml')

    for n in g.nodes():
        if 'id' in g.node[n]:
            g.node[n].pop('id')

    for n in g.nodes():
        graphics = g.node[n].pop('graphics')
        g.node[n]['x'] = graphics['x']
        g.node[n]['y'] = graphics['y']

    json_graph.dump(g, open('../../jdk_gelphi_radial.json', 'w'))


