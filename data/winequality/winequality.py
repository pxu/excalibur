import csv
import json
import os


if __name__ == '__main__':
    data = {}
    with open('winequality-white.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
        for dataItem in reader:
            for k in dataItem:
                if not k in data:
                    data[k] = []
                else:
                    data[k].append(float(dataItem[k]))

    with open('winequality-white.json', 'w') as jsonfile:
        js_str = '['
        for k in data:
            js_str += '{' + 'attribute: ' + k + ',' + 'values: ' + '['
            for d in data[k]:
                js_str += str(d) + ','
            js_str = js_str[:-1]
            js_str += ']' + '},'
        js_str = js_str[:-1]
        js_str += ']'
        jsonfile.write(js_str)

    for k in data:
        print(k + ": " + "max = " + str(max(data[k])) + '; ' + "min = " + str(min(data[k])))






