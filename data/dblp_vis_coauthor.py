if __name__ == '__main__':
    from networkx.readwrite import json_graph
    import networkx as nx

    g0 = nx.read_graphml('dblp_vis_coauthor_layout.graphml')

    unused_attrs = ['size', 'r', 'g', 'b', 'z']
    for n in g0.nodes():
        for attr in unused_attrs:
            if attr in g0.node[n]:
                g0.node[n].pop(attr)

    with open('dblp_vis_coauthor.json', 'w') as jsonfile:
        json_graph.dump(g0, jsonfile)


    g0 = json_graph.load(open('dblp_vis_coauthor.json', 'r'))

    attrs = ['x', 'y']

    with open('dblp_vis_coauthor_layout.json', 'w') as jsonfile:
        js_str = '['
        for attr in attrs:
            js_str += '{' + 'attribute: \"' + attr + '\",' + 'values: ' + '['
            for n in g0.nodes():
                js_str += str(g0.node[n][attr]) + ','
            js_str = js_str[:-1]
            js_str += ']' + '},'
        js_str = js_str[:-1]
        js_str += ']'
        jsonfile.write(js_str)






